﻿#region Namespaces for BankAppDesign

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data.OleDb;
using System.Data;
using System.Globalization;

#region Namespaces for reference to CRM classes

using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Tooling.Connector;

#endregion

#endregion


namespace ValuaTravel.BankApp
{
    public partial class BankStatementUpload : System.Web.UI.Page
    {
        #region Declaration of Global variables
        static IOrganizationService service;
        DataTable dynamicTable = new DataTable();
        static DataTable sheetData = new DataTable();
        static Guid bankStatementId = Guid.Empty;
        string FilePath = string.Empty;
        static string[] statementDateData;
        static Guid currentCRMUserId = Guid.Empty;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //Get Organization Service
            if (IsPostBack == false)
            {
                service = getService();
                string userId = "D2D521A8-BE84-E211-A130-18A9057470F6"; //Request.QueryString["id"].ToString(); //    "D2D521A8-BE84-E211-A130-18A9057470F6";
                currentCRMUserId = new Guid(userId);
            }
        }

        #region*************Retrieve Organization Service********************************
        public static IOrganizationService getService()
        {
            //ServerConnection serverConnect = new ServerConnection();
            //ServerConnection.Configuration config = serverConnect.GetServerConfiguration();
            //OrganizationServiceProxy _ServiceProxy;
            //_ServiceProxy = new OrganizationServiceProxy(config.OrganizationUri,
            //                                             config.HomeRealmUri,
            //                                             config.Credentials,
            //                                             config.DeviceCredentials) { Timeout = TimeSpan.FromMilliseconds(200000) };
            //_ServiceProxy.ServiceConfiguration.CurrentServiceEndpoint.Behaviors.Add(new ProxyTypesBehavior());
            //IOrganizationService service = (IOrganizationService)_ServiceProxy;


            CrmServiceClient conn = new Microsoft.Xrm.Tooling.Connector.CrmServiceClient(ConfigurationManager.ConnectionStrings["conString"].ToString());
            service = (IOrganizationService)conn.OrganizationWebProxyClient != null ? (IOrganizationService)conn.OrganizationWebProxyClient : (IOrganizationService)conn.OrganizationServiceProxy;
                     
            return service;
        }
        #endregion

        #region Uploaded file processing and Upload Statement button execution
        protected void uploadButton_Click(object sender, EventArgs e)
        {
            updateBankStatementinCRM.Enabled = false;
            uploadButton.Enabled = false;
            bankStatementUploader.Enabled = false;
            string Extension = Path.GetExtension(bankStatementUploader.PostedFile.FileName);
            if (bankStatementUploader.HasFile && Extension == ".xlsx" || Extension == ".xls")
            {
                try
                {
                    string FileName = Path.GetFileName(bankStatementUploader.PostedFile.FileName);
                    string FolderPath = ConfigurationManager.AppSettings["FolderPath"];
                    string FilePath = Server.MapPath("/Files/") + FileName;                  
                    bankStatementUploader.SaveAs(FilePath);
                    Import_To_Grid(FilePath, Extension, "No");
                }
                catch (Exception ex)
                {
                    string statusofService = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                    ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Upload status: The file could not be uploaded.');", true);
                    uploadButton.Enabled = true;
                    bankStatementUploader.Enabled = true;
                    File.Delete(FilePath);

                }
            }
            else
            {
                if (!bankStatementUploader.HasFile)
                    ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Please select file to upload.')", true);
                else
                    ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('Please select valid file to upload.')", true);
                uploadButton.Enabled = true;
                bankStatementUploader.Enabled = true;
            }
        }

        #endregion

        #region Create Columns add dynamic rows to grid view
        private void FirstGridViewRow(bool flag, string fechaOper, string concepto, string fechaValor, string importe, string saldo, string referencia1, string referencia2, bool allRows)
        {
            DataRow dr = null;
            if (flag == false)
            {
                dynamicTable.Columns.Add(new DataColumn("FECHA OPER", typeof(string)));
                dynamicTable.Columns.Add(new DataColumn("CONCEPTO", typeof(string)));
                dynamicTable.Columns.Add(new DataColumn("FECHA VALOR", typeof(string)));
                dynamicTable.Columns.Add(new DataColumn("IMPORTE", typeof(string)));
                dynamicTable.Columns.Add(new DataColumn("SALDO", typeof(string)));
                dynamicTable.Columns.Add(new DataColumn("REFERENCIA 1", typeof(string)));
                dynamicTable.Columns.Add(new DataColumn("REFERENCIA 2", typeof(string)));
            }
            else
            {
                dr = dynamicTable.NewRow();
                dr["FECHA OPER"] = fechaOper;
                dr["CONCEPTO"] = concepto;
                dr["FECHA VALOR"] = fechaValor;
                dr["IMPORTE"] = importe;
                dr["SALDO"] = saldo;
                dr["REFERENCIA 1"] = referencia1;
                dr["REFERENCIA 2"] = referencia2;
                dynamicTable.Rows.Add(dr);
            }
            if (allRows == true)
            {
                bankStatementUploadGrid.DataSource = dynamicTable;
                bankStatementUploadGrid.DataBind();
            }
        }
        #endregion

        #region Display the data in Grid View, which we will import to Bank Statement Detail Entity & Show Account Name, Title of Statement, Statement Export Date
        private void Import_To_Grid(string FilePath, string Extension, string isHDR)
        {
            
            string conStr = "";
            try
            {
             
               sheetData.Rows.Clear();

                //switch (Extension)
                //{
                //    case ".xls": //Excel 97-03
                //        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"]
                //                 .ConnectionString;

                //        break;
                //    case ".xlsx": //Excel 07
                //        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"]
                //                  .ConnectionString;
                //        break;
                //}
               
                conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                conStr = String.Format(conStr, FilePath, isHDR);

                OleDbConnection connExcel = new OleDbConnection(conStr);

                OleDbCommand cmdExcel = new OleDbCommand();
                OleDbDataAdapter oda = new OleDbDataAdapter();
                cmdExcel.Connection = connExcel;

               

                //Get the name of First Sheet
                connExcel.Open();
                
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
           
                string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                connExcel.Close();
              
                //Read Data from First Sheet
                connExcel.Open();
                cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                cmdExcel.Connection = connExcel;
                oda.SelectCommand = cmdExcel;
                oda.Fill(sheetData);
                connExcel.Close();
               
                DataTable tableForGrid = new DataTable();
                if (sheetData.Rows.Count > 0)
                {
                    
                   
                    string cuenta, currency, title;

                    //Statement Date show on Label
                    statementDateData = (sheetData.Rows[0][0].ToString()).Split('s');
                    statementDate.Text = "<b>Statement Date:</b>" + statementDateData[2].ToString().Trim();

                    //Account show on Label
                    cuenta = sheetData.Rows[1][1].ToString();
                    accountName.Text = "<b>Cuenta: </b>" + cuenta;

                    //Currency show on Label
                    currency = sheetData.Rows[2][1].ToString();
                    currenyCode.Text = "<b>Divisa: </b>" + currency;

                    //Title of Statement show on Label
                    title = sheetData.Rows[3][1].ToString();
                    statementTitle.Text = "<b>Titular: </b>" + title;

                    //Data bind to Grid View Control
                    FirstGridViewRow(false, null, null, null, null, null, null, null, false);
                    for (int index = 6; index <= sheetData.Rows.Count - 1; index++)
                    {
                        DataRow recordRow = sheetData.Rows[index];
                        if (index == sheetData.Rows.Count - 1)
                            FirstGridViewRow(true, recordRow[0].ToString(), recordRow[1].ToString(), recordRow[2].ToString(), recordRow[3].ToString(), recordRow[4].ToString(), recordRow[5].ToString(), recordRow[6].ToString(), true);
                        else
                            FirstGridViewRow(true, recordRow[0].ToString(), recordRow[1].ToString(), recordRow[2].ToString(), recordRow[3].ToString(), recordRow[4].ToString(), recordRow[5].ToString(), recordRow[6].ToString(), false);
                    }
                    updateBankStatementinCRM.Enabled = true;

                    string statusofService = dynamicTable.Rows.Count + " records are successfully uploaded to Webserver.";
                    ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('" + statusofService + "');", true);
                    File.Delete(FilePath);
                }

                
            }
            catch (Exception ex) 
            {
                string statusofService = "File processing status: The file could not be processed. The following error occured: " + ex.Message;
                ClientScript.RegisterStartupScript(GetType(), "text", "alert('File processing status: The file could not be processed.');", true);
                File.Delete(FilePath);
                uploadButton.Enabled = true;
                bankStatementUploader.Enabled = true;
            }
        }
        #endregion

        #region Create records of Bank Statement and Bank Statement Detail for current uploaded bank statement
        protected void updateBankStatementinCRM_Click(object sender, EventArgs e)
        {
            int created = 0, notCreated = 0;
            uploadButton.Enabled = false;
            bankStatementUploader.Enabled = false;
            string statusofService = string.Empty;
            try
            {
                IFormatProvider culture = new System.Globalization.CultureInfo("es-ES", true);
                string st = statementDateData[2].ToString().Trim();
                DateTime statementDate = DateTime.ParseExact(st, "dd/MM/yyyy HH:mm:ss", culture);

                //Check whether statement sheet is not the existing one by matching Statement Time and Date with Existing records.

                string fetchStatementCheck = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                              <entity name='dfi_bankstatements'>
                                                <attribute name='dfi_bankstatementsid' />
                                                <attribute name='dfi_name' />
                                                <attribute name='createdon' />
                                                <order attribute='dfi_name' descending='false' />
                                                <filter type='and'>
                                                  <condition attribute='dfi_date' operator='on' value='" + statementDate + @"'/>
                                                </filter>
                                              </entity>
                                            </fetch>";

                EntityCollection collectionStatementCheck = service.RetrieveMultiple(new FetchExpression(fetchStatementCheck));
                if (collectionStatementCheck.Entities.Count <= 0)
                {
                    int uploadId = 1;
                    string fetchHighestUploadId = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='true'>
                                              <entity name='dfi_bankstatements'>
                                                <attribute name='dfi_bankstatementsid' />
                                                <attribute name='dfi_name' />
                                                <attribute name='createdon' />
                                                <order attribute='dfi_name' descending='true' />
                                              </entity>
                                            </fetch>";
                    EntityCollection highestUploadIdCollection = service.RetrieveMultiple(new FetchExpression(fetchHighestUploadId));
                    if (highestUploadIdCollection.Entities.Count > 0)
                    {
                        if (highestUploadIdCollection.Entities[0].Contains("dfi_name") && highestUploadIdCollection.Entities[0]["dfi_name"] != null)
                            uploadId = Convert.ToInt32(highestUploadIdCollection.Entities[0]["dfi_name"]) + 1;
                    }

                    Entity bankStatement = new Entity("dfi_bankstatements");
                    bankStatement["dfi_name"] = uploadId.ToString();
                    bankStatement["dfi_account"] = sheetData.Rows[1][1].ToString();
                    bankStatement["dfi_date"] = statementDate;
                    bankStatement["dfi_selection"] = sheetData.Rows[4][1].ToString();
                    bankStatementId = service.Create(bankStatement); 
                     assignRecordsToUser("dfi_bankstatements", bankStatementId);
                    if (bankStatementId != Guid.Empty)
                    {
                        foreach (GridViewRow recordRow in bankStatementUploadGrid.Rows)
                        {
                            string errormsg = "";
                            string concepto;
                            decimal importe, saldo;

                            DateTime fechaOper, fechaValor;
                            errormsg = recordRow.Cells[0].Text;
                            fechaOper = DateTime.ParseExact(recordRow.Cells[0].Text, "dd/MM/yyyy",  culture);
                            errormsg += fechaOper.ToString();
                            concepto = recordRow.Cells[1].Text;
                            fechaValor = DateTime.ParseExact(recordRow.Cells[2].Text, "dd/MM/yyyy", culture);
                            importe = Convert.ToDecimal(recordRow.Cells[3].Text);
                            saldo = Convert.ToDecimal(recordRow.Cells[4].Text);

                            //Check whether the same record for Bank Statement Detail Exists in CRM or not.

                            string fetchRecordExist = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                  <entity name='dfi_bankstatementsdetails'>
                                                    <attribute name='dfi_bankstatementsdetailsid' />
                                                    <attribute name='dfi_name' />
                                                    <attribute name='createdon' />
                                                    <order attribute='dfi_name' descending='false' />
                                                    <filter type='and'>
                                                      <condition attribute='dfi_fechaoper' operator='on' value='" + (fechaOper.GetDateTimeFormats())[5] + @"'/>
                                                      <condition attribute='dfi_importe' operator='eq' value='" + importe + @"' />
                                                      <condition attribute='dfi_saldo' operator='eq' value='" + saldo + @"' />
                                                    </filter>
                                                  </entity>
                                                </fetch>";
                        
                            EntityCollection checkWheterBankStmtDtlExist = service.RetrieveMultiple(new FetchExpression(fetchRecordExist));
                            if (checkWheterBankStmtDtlExist.Entities.Count <= 0)
                            {
                                Entity bankStatementDetail = new Entity("dfi_bankstatementsdetails");
                                bankStatementDetail["dfi_uploadid"] = new EntityReference("dfi_bankstatements", bankStatementId);
                                EntityReference clientProveedor = null;
                                string excludedTextClientProveedor = excludeTextFromConcepto(concepto.ToString()).ToString();
                                if (excludedTextClientProveedor == string.Empty)
                                {
                                    clientProveedor = selectClientProveedorforConcepto(concepto);
                                    bankStatementDetail["dfi_statementproveedor"] = concepto.ToString();
                                    bankStatementDetail["dfi_clientproveedorid"] = clientProveedor;
                                }
                                else
                                {
                                    clientProveedor = getClientProveedor(excludedTextClientProveedor);
                                    bankStatementDetail["dfi_statementproveedor"] = excludedTextClientProveedor;
                                    bankStatementDetail["dfi_clientproveedorid"] = clientProveedor;
                                }
                                bankStatementDetail["dfi_fechaoper"] = fechaOper; 
                                 bankStatementDetail["dfi_fechavalor"] = fechaValor;
                                bankStatementDetail["dfi_importe"] = new Money(importe);
                                bankStatementDetail["dfi_saldo"] = new Money(saldo);                                
                                bankStatementDetail["dfi_appliedtoclientproveedor"] = false;
                                Guid bnkStatementDetailId = service.Create(bankStatementDetail);
                                assignRecordsToUser("dfi_bankstatementsdetails", bnkStatementDetailId);
                                created = created + 1;
                            }
                            else
                                notCreated = notCreated + 1;
                        }

                        Entity updatebank = new Entity("dfi_bankstatements");
                        updatebank["dfi_totalcount"] = created;
                        updatebank.Id = bankStatementId;
                        service.Update(updatebank);


                    }
                    statusofService = "The Bank Statement is uploaded to CRM Successfully. ";
                    string statusofBankDetailCreated = string.Empty;
                    string statusofBankDetailNotCreated = string.Empty;
                    if (created == 0)
                        statusofBankDetailCreated = "No new record created for this statement in CRM. Please check, Is this file the old one?";
                    else if (created > 0)
                        statusofBankDetailCreated = created.ToString() + " new records are created for statements.";
                    if (notCreated != 0)
                        statusofBankDetailNotCreated = notCreated.ToString() + " records are not created in CRM";

                    statusofService = statusofService + statusofBankDetailCreated + statusofBankDetailNotCreated;
                    ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('" + statusofService + "');", true);
                }
                else
                {
                    statusofService = "This file is already uploaded to CRM";
                    ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('" + statusofService + "');", true);
                }
                updateBankStatementinCRM.Enabled = false;
                bankStatementUploader.Enabled = true;
                uploadButton.Enabled = true;
            }
            catch (Exception ex)
            {
                statusofService = "Upload to CRM Status: The file could not be uploaded to CRM. The following error occured: " + ex.Message;
                ClientScript.RegisterStartupScript(GetType(), "hwa", "alert('" + statusofService + "');", true);
            }
        }
        #endregion

        #region Assign Bank Statement and Bank Statement Details record to Current logged in user of CRM
        public void assignRecordsToUser(string entityLogicalName, Guid recordId)
        {
            Guid userId = currentCRMUserId;
            if (userId != Guid.Empty)
            {
                AssignRequest assigneRecordstoCurrentUser = new AssignRequest();
                assigneRecordstoCurrentUser.Assignee = new EntityReference("systemuser", userId);
                assigneRecordstoCurrentUser.Target = new EntityReference(entityLogicalName, recordId);
                service.Execute(assigneRecordstoCurrentUser);
            }
        }
        #endregion

        #region Bank Statement Configuration entity procession. Exclude text from Concepto in Banck Statement Detail entity.
        private string excludeTextFromConcepto(string concepto)
        {
            string processedConcepto = string.Empty;
            string fetchBankSttmntConfig = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                <entity name='dfi_bankstatementconfiguration'>
                                                <attribute name='dfi_bankstatementconfigurationid' />
                                                <attribute name='dfi_name' />
                                                <attribute name='createdon' />
                                                <order attribute='dfi_name' descending='false' />
                                                <filter type='and'>
                                                    <condition attribute='dfi_exclude' operator='eq' value='1' />
                                                </filter>
                                                </entity>
                                            </fetch>";

            EntityCollection collectionBankSttmntConfig = service.RetrieveMultiple(new FetchExpression(fetchBankSttmntConfig));
            if (collectionBankSttmntConfig.Entities.Count > 0)
            {
                foreach (Entity bnkSttmntConfig in collectionBankSttmntConfig.Entities)
                {
                    string wordToExclude = bnkSttmntConfig["dfi_name"].ToString();
                    if (concepto.Contains(wordToExclude))
                    {
                        concepto = concepto.Replace(wordToExclude, "");
                        concepto = concepto.Trim();
                        processedConcepto = concepto;
                    }
                }
            }
            return processedConcepto;
        }
        #endregion

        #region Select Client/Proveedor from Bank Statement Configuration entity if Exclude is not Yes
        private EntityReference selectClientProveedorforConcepto(string concepto)
        {
            EntityReference clientProveedorRef = null;
            string fetchBankSttmntConfig = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                              <entity name='dfi_bankstatementconfiguration'>
                                                <attribute name='dfi_bankstatementconfigurationid' />
                                                <attribute name='dfi_name' />
                                                <attribute name='createdon' />
                                               <attribute name='dfi_clientproveedorid'/>
                                                <order attribute='dfi_name' descending='false' />
                                                <filter type='and'>
                                                  <condition attribute='dfi_exclude' operator='eq' value='0' />
                                                </filter>
                                              </entity>
                                            </fetch>";

            EntityCollection collectionBankSttmntConfig = service.RetrieveMultiple(new FetchExpression(fetchBankSttmntConfig));
            if (collectionBankSttmntConfig.Entities.Count > 0)
            {
                foreach (Entity bnkSttmntConfig in collectionBankSttmntConfig.Entities)
                {
                    if (bnkSttmntConfig.Contains("dfi_name") && bnkSttmntConfig["dfi_name"] != null)
                    {
                        string statementProveedor = bnkSttmntConfig["dfi_name"].ToString();
                        concepto = concepto.Trim();
                        if (concepto.Contains(statementProveedor))
                        {
                            if (bnkSttmntConfig.Contains("dfi_clientproveedorid") && bnkSttmntConfig["dfi_clientproveedorid"] != null)
                                clientProveedorRef = (EntityReference)bnkSttmntConfig["dfi_clientproveedorid"];
                        }
                    }
                }
            }
            return clientProveedorRef;
        }
        #endregion

        #region Get Client/Proveedor for those recodes which have excluded text
        private EntityReference getClientProveedor(string fiscalName)
        {
            EntityReference clientProveedor = null;
            string fetchClientProveedor = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                                  <entity name='account'>
                                                    <attribute name='name' />
                                                    <attribute name='primarycontactid' />
                                                    <attribute name='telephone1' />
                                                    <attribute name='accountid' />
                                                    <order attribute='name' descending='false' />
                                                    <filter type='and'>
                                                      <condition attribute='new_nombrefiscal' operator='eq' value='" + fiscalName + @"' />
                                                    </filter>
                                                  </entity>
                                                </fetch>";
            EntityCollection collectionClientCollection = service.RetrieveMultiple(new FetchExpression(fetchClientProveedor));
            if (collectionClientCollection.Entities.Count > 0)
            {
                Entity clientProveedorEntity = collectionClientCollection.Entities[0];
                if (clientProveedorEntity.Id != Guid.Empty)
                {
                    clientProveedor = new EntityReference("account", clientProveedorEntity.Id);
                }
            }
            return clientProveedor;
        }
        #endregion
    }
}