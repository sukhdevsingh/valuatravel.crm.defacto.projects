﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BankStatementUpload.aspx.cs" Inherits="ValuaTravel.BankApp.BankStatementUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div>
                <h1 style="text-align:center">
                    <asp:ScriptManager ID="ScriptManager1" runat="server">
                    </asp:ScriptManager>
                    BANK STATEMENT UPLOAD</h1>
            </div>
        <div style="display:block">
            <asp:Label ID="statementDate" runat="server"></asp:Label>
            <br />
            <asp:Label ID="accountName" runat="server"></asp:Label>
            <br />
            <asp:Label id="currenyCode" runat="server"></asp:Label>
            <br />
            <asp:Label ID="statementTitle" runat="server"></asp:Label>
        </div>
        <div style="text-align:center">
            <asp:FileUpload ID="bankStatementUploader" runat="server" /> 
            &nbsp;<asp:Button ID="uploadButton" runat="server" Text="Upload Statement" OnClick="uploadButton_Click" />
            &nbsp;<asp:Button ID="updateBankStatementinCRM" runat="server" Text="Upload to CRM" OnClick="updateBankStatementinCRM_Click" Enabled="false"/>
            <br />
            <br />
            <div style="text-align:center; margin:0% 5% 0% 5%">
                <br />
                <br />
            <asp:GridView ID="bankStatementUploadGrid" runat="server">
            </asp:GridView>                
             </div>
            </div>
    </div>
    </form>
</body>
</html>
