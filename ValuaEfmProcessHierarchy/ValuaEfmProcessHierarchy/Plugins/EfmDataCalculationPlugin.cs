// <copyright file="EfmDataCalculationPlugin.cs" company="">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author></author>
// <date>12/5/2016 2:31:40 PM</date>
// <summary>Implements the EfmDataCalculationPlugin Plugin.</summary>
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
// </auto-generated>
namespace ValuaEfmProcessHierarchy.Plugins
{
    using System;
    using System.ServiceModel;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;

    /// <summary>
    /// EfmDataCalculationPlugin Plugin.
    /// </summary>    
    public class EfmDataCalculationPlugin : Plugin
    {
        string EfmYear = "";
        decimal totalOrderPVP = 0;
        decimal totalOrderProductPVP = 0;
        decimal totalmgnetOrder = 0;
        decimal totalmgnetOrderProduct = 0;

        decimal totalmrgenettotalOfertas = 0;
        decimal totalPvpOfertas = 0;
        IOrganizationService service;
        /// <summary>
        /// Initializes a new instance of the <see cref="EfmDataCalculationPlugin"/> class.
        /// </summary>
        public EfmDataCalculationPlugin()
            : base(typeof(EfmDataCalculationPlugin))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "dfi_EFMDataCalculation", "new_efm", new Action<LocalPluginContext>(ExecuteEfmDataCalculationPlugin)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteEfmDataCalculationPlugin(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            service = localContext.OrganizationService;

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
            {
                EntityReference Target = (EntityReference)context.InputParameters["Target"];
                Guid EfmId = Target.Id;
                Entity Efm = service.Retrieve("new_efm", Target.Id, new ColumnSet(true));
                EfmYear = Efm.Attributes.Contains("new_year") ? Efm.FormattedValues["new_year"].ToString() : "";

                //Update Total estimado , Total real and total difference in EFM record.
                UpdateEfmRecord_AggregareEfmSummary(EfmId);

                GetDatafromallOrderByEfmYear(EfmYear);
                GetDatafromallOfertasByEfmYear(EfmYear);
                
                //Total Cost Estimado
                decimal totalCostEstimado = Efm.Attributes.Contains("new_total") && Efm.Attributes["new_total"] != null ? ((Money)Efm.Attributes["new_total"]).Value : 0;
                //Total Cost Real
                decimal totalCostreal = Efm.Attributes.Contains("dfi_totalreal") && Efm.Attributes["dfi_totalreal"] != null ? ((Money)Efm.Attributes["dfi_totalreal"]).Value : 0;
                //BAI estimado
                decimal BAIEstimado = Efm.Attributes.Contains("new_benefici") && Efm.Attributes["new_benefici"] != null ? ((Money)Efm.Attributes["new_benefici"]).Value : 0;


                //Manual1
                decimal facturacionEstimado =Efm.Attributes.Contains("dfi_facturacionestimado") && Efm.Attributes["dfi_facturacionestimado"]!=null ? ((Money)Efm.Attributes["dfi_facturacionestimado"]).Value : 0;
                //Manual2
                decimal explotacionnetopedidosestimado = Efm.Attributes.Contains("dfi_explotacionnetopedidosestimado_d") && Efm.Attributes["dfi_explotacionnetopedidosestimado_d"] != null ? (decimal)Efm.Attributes["dfi_explotacionnetopedidosestimado_d"] : 0;

                decimal facturacionDifferenciaReal = facturacionEstimado - Math.Abs(totalOrderPVP);
                
                Entity _updateEfmRecord = new Entity("new_efm");
                //Value 0
                _updateEfmRecord.Attributes["dfi_facturacionreal"] = new Money(totalOrderPVP);
                //Value 9
                _updateEfmRecord.Attributes["dfi_facturaciondiferenciareal"] = new Money(facturacionDifferenciaReal);
                //Value 12
                _updateEfmRecord.Attributes["dfi_facturacionprevventas"] = new Money(totalOrderProductPVP);
                //Value 13
                _updateEfmRecord.Attributes["dfi_facturacindiferenciaprev"] = new Money(facturacionEstimado- totalOrderProductPVP);
                //Value 14
                _updateEfmRecord.Attributes["dfi_bexplotacionpedidosestimado"] = new Money((facturacionEstimado * explotacionnetopedidosestimado)/100);
                //Value 5
                _updateEfmRecord.Attributes["dfi_bexplotacionpedidosreal"] = new Money(totalmgnetOrder);
                //Value 6
                _updateEfmRecord.Attributes["dfi_bexplotacionpedidosdiferenciareal"] = new Money(totalmgnetOrder - ((facturacionEstimado * explotacionnetopedidosestimado) / 100));
                //Value 4
                _updateEfmRecord.Attributes["dfi_bexplotacionpedidosprevventas"] = new Money(totalmgnetOrderProduct);
                //Value 15
                _updateEfmRecord.Attributes["dfi_bexplotacionpedidosdiferenciaprev"] = new Money(totalmgnetOrderProduct- ((facturacionEstimado * explotacionnetopedidosestimado) / 100));
                //Value 1
                _updateEfmRecord.Attributes["dfi_explotacionnetopedidosreal_d"] = Convert.ToDecimal((totalmgnetOrder/ totalOrderPVP)*100);
                //Value 10
                _updateEfmRecord.Attributes["dfi_explotacionnetopedidosdiferenciareal_d"] = Convert.ToDecimal(((totalmgnetOrder / totalOrderPVP) * 100)- explotacionnetopedidosestimado);
                //Value 8
                _updateEfmRecord.Attributes["dfi_explotacionnetopedidosprevventas_d"] = Convert.ToDecimal(((totalmgnetOrderProduct / totalOrderProductPVP) * 100));
                //Value 11
                _updateEfmRecord.Attributes["dfi_explotacionnetopedidosdiferenciaprev_d"] = Convert.ToDecimal(((totalmgnetOrderProduct / totalOrderProductPVP) * 100) - explotacionnetopedidosestimado);
                //Value 6
                _updateEfmRecord.Attributes["dfi_bexplotacionofertasofertasreal"] = new Money(totalmrgenettotalOfertas);
                //Value 7
                _updateEfmRecord.Attributes["dfi_explotacionnetoofertasreal_d"] = Convert.ToDecimal((totalmrgenettotalOfertas/totalPvpOfertas)*100);                               
                //Value 2
                _updateEfmRecord.Attributes["dfi_pmbaireal"] = new Money(Math.Abs(Math.Abs(totalCostreal) + BAIEstimado));
                //Value 16
                _updateEfmRecord.Attributes["dfi_pmbaiprevventas"] = new Money (totalCostEstimado+ BAIEstimado);
                
                _updateEfmRecord.Id = EfmId;
                service.Update(_updateEfmRecord);

            }
        }

        private void UpdateEfmRecord_AggregareEfmSummary(Guid EfmId)
        {
            decimal totalBudget = 0;
            decimal totalActual= 0;
            decimal totalDiff= 0;
            var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' >
                              <entity name='dfi_efmsummary'>
                                <attribute name='dfi_efmsummaryid' />
                                <attribute name='dfi_name' />
                                <attribute name='createdon' />
                                <attribute name='dfi_actual' />
                                <attribute name='dfi_budget' />
                                <attribute name='dfi_difference' />
                                <filter type='and'>
                                  <condition attribute='dfi_efmid' operator='eq' value='" + EfmId + @"' />
                                </filter>
                              </entity>
                            </fetch>";
            EntityCollection coll = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (coll != null && coll.Entities.Count > 0)
            {
                foreach (var record in coll.Entities)
                {
                    Money budget = record.Attributes.Contains("dfi_budget") && record.Attributes["dfi_budget"] != null ? (Money)record.Attributes["dfi_budget"] : new Money(0);
                    Money actual = record.Attributes.Contains("dfi_actual") && record.Attributes["dfi_actual"] != null ? (Money)record.Attributes["dfi_actual"] : new Money(0);
                    Money diff = record.Attributes.Contains("dfi_difference") && record.Attributes["dfi_difference"] != null ? (Money)record.Attributes["dfi_difference"] : new Money(0);

                    totalBudget = totalBudget + budget.Value;
                    totalActual = totalActual + actual.Value;
                    totalDiff = totalDiff + diff.Value;
                }

                Entity UpdateEfm = new Entity("new_efm");
                UpdateEfm.Attributes["new_total"] = new Money(totalBudget);
                UpdateEfm.Attributes["dfi_totalreal"] = new Money(totalActual);
                UpdateEfm.Attributes["dfi_totaldiferenciareal"] = new Money(totalDiff);
                UpdateEfm.Id = EfmId;
                service.Update(UpdateEfm);
            }
        }

        private void GetDatafromallOrderByEfmYear(string EfmYear)
        {
            var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                              <entity name='salesorder'>
                                <attribute name='name' />
                                <attribute name='customerid' />
                                <attribute name='statuscode' />
                                <attribute name='new_stunitgrandtotal' />
                                <attribute name='new_ctegrandtotal' />
                                <attribute name='dfi_prebooking' />
                                <attribute name='salesorderid' />
                                <attribute name='new_pagadocobros' />
                                <attribute name='new_stunittotal' /> 
                                <attribute name='new_mgenetperpaxcierre' />
                                <attribute name='new_mrgenettotal' />
                                <order attribute='name' descending='false' />
                                <filter type='and'>
                                <condition attribute='new_fechasalida' operator='on-or-after' value='" + EfmYear+@"-01-01' />
                                <condition attribute='new_fechasalida' operator='on-or-before' value='"+EfmYear+@"-12-31' />
                                </filter>
                              </entity>
                            </fetch>";
            EntityCollection coll = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (coll != null && coll.Entities.Count > 0)
            {
                foreach (var record in coll.Entities)
                {
                    Money OrderPVP = record.Attributes.Contains("new_pagadocobros") && record.Attributes["new_pagadocobros"] != null ? (Money)record.Attributes["new_pagadocobros"] : new Money(0);
                    Money OrderProductPVP = record.Attributes.Contains("new_stunittotal") && record.Attributes["new_stunittotal"] != null ? (Money)record.Attributes["new_stunittotal"] : new Money(0);
                    Money mgnetperpaxcierre = record.Attributes.Contains("new_mgenetperpaxcierre") && record.Attributes["new_mgenetperpaxcierre"] != null ? (Money)record.Attributes["new_mgenetperpaxcierre"] : new Money(0);
                    Money mrgnettotal = record.Attributes.Contains("new_mrgenettotal") && record.Attributes["new_mrgenettotal"] != null ? (Money)record.Attributes["new_mrgenettotal"] : new Money(0);

                    totalOrderPVP = totalOrderPVP + OrderPVP.Value;
                    totalOrderProductPVP = totalOrderProductPVP + OrderProductPVP.Value;
                    totalmgnetOrder = totalmgnetOrder + mgnetperpaxcierre.Value;
                    totalmgnetOrderProduct = totalmgnetOrderProduct + mrgnettotal.Value;
                }
            }
        }

        private void GetDatafromallOfertasByEfmYear(string EfmYear)
        {
            var fetchXml = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                              <entity name='quote'>
                                <attribute name='customerid' />
                                <attribute name='statecode' />
                                <attribute name='createdon' />
                                <attribute name='quoteid' />
                                <attribute name='quotenumber' />
                                <attribute name='new_pvp' />
                                <attribute name='new_paxacomp' />
                                <attribute name='new_pax' />
                                <attribute name='new_mrgenettotal' />
                                <attribute name='new_stunitgrandtotal' />
                                <attribute name='new_fechasalida' />
                                <order attribute='createdon' descending='true' />
                                <order attribute='quotenumber' descending='false' />
                                <filter type='and'>
                                <condition attribute='new_fechasalida' operator='on-or-after' value='" + EfmYear + @"-01-01' />
                                <condition attribute='new_fechasalida' operator='on-or-before' value='" + EfmYear + @"-12-31' />
                                <condition attribute='statecode' operator='eq' value='2' />
                                </filter>
                              </entity>
                            </fetch>";
            EntityCollection coll = service.RetrieveMultiple(new FetchExpression(fetchXml));
            if (coll != null && coll.Entities.Count > 0)
            {
                foreach (var record in coll.Entities)
                {
                   Money mrgenettotal= record.Attributes.Contains("new_mrgenettotal") && record.Attributes["new_mrgenettotal"] != null ? (Money)record.Attributes["new_mrgenettotal"] : new Money(0);
                    Money pvptotal= record.Attributes.Contains("new_stunitgrandtotal") && record.Attributes["new_stunitgrandtotal"] != null ? (Money)record.Attributes["new_stunitgrandtotal"] : new Money(0);
                    totalmrgenettotalOfertas = totalmrgenettotalOfertas + mrgenettotal.Value;
                    totalPvpOfertas = totalPvpOfertas + pvptotal.Value;

                }
            }
        }
    }
}
