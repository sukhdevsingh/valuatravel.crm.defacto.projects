// <copyright file="EfmProcessPlugin.cs" company="">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author></author>
// <date>11/21/2016 6:59:11 PM</date>
// <summary>Implements the EfmProcessPlugin Plugin.</summary>
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.1
// </auto-generated>
namespace ValuaEfmProcessHierarchy.Plugins
{
    using System;
    using System.ServiceModel;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;

    /// <summary>
    /// EfmProcessPlugin Plugin.
    /// </summary>    
    public class EfmProcessPlugin: Plugin
    {
        Entity Efm;
        Guid EfmId = Guid.Empty;
        string EfmYear = "";
        IOrganizationService service;
        /// <summary>
        /// Initializes a new instance of the <see cref="EfmProcessPlugin"/> class.
        /// </summary>
        public EfmProcessPlugin()
            : base(typeof(EfmProcessPlugin))
        {
            base.RegisteredEvents.Add(new Tuple<int, string, string, Action<LocalPluginContext>>(40, "dfi_EfmProcessHierarchyAction", "new_efm", new Action<LocalPluginContext>(ExecuteEfmProcessPlugin)));

            // Note : you can register for more events here if this plugin is not specific to an individual entity and message combination.
            // You may also need to update your RegisterFile.crmregister plug-in registration file to reflect any change.
        }

        /// <summary>
        /// Executes the plug-in.
        /// </summary>
        /// <param name="localContext">The <see cref="LocalPluginContext"/> which contains the
        /// <see cref="IPluginExecutionContext"/>,
        /// <see cref="IOrganizationService"/>
        /// and <see cref="ITracingService"/>
        /// </param>
        /// <remarks>
        /// For improved performance, Microsoft Dynamics CRM caches plug-in instances.
        /// The plug-in's Execute method should be written to be stateless as the constructor
        /// is not called for every invocation of the plug-in. Also, multiple system threads
        /// could execute the plug-in at the same time. All per invocation state information
        /// is stored in the context. This means that you should not use global variables in plug-ins.
        /// </remarks>
        protected void ExecuteEfmProcessPlugin(LocalPluginContext localContext)
        {
            if (localContext == null)
            {
                throw new ArgumentNullException("localContext");
            }

            IPluginExecutionContext context = localContext.PluginExecutionContext;
            service = localContext.OrganizationService;

            if (context.InputParameters.Contains("Target") && context.InputParameters["Target"] is EntityReference)
            {
                EntityReference Target = (EntityReference)context.InputParameters["Target"];
                EfmId = Target.Id;
                Efm = service.Retrieve("new_efm", Target.Id, new ColumnSet(true));

                Entity _efmUpdate = new Entity("new_efm");
                _efmUpdate.Id = EfmId;

                EfmYear = Efm.Attributes.Contains("new_year") ? Efm.FormattedValues["new_year"].ToString():"";

                DeleteExistingEfmSummaryRecord(EfmId);

                #region Efm Budget && Actual for "641-Gastos de Personal"

                int _641SeqNum = 1;
                string _641text = "641-Gastos de Personal";                
                int _641Value = GetOptionsSetValueGivenText(service, "dfi_proveedortype", "dfi_accounttype", _641text);
                EntityCollection _641Coll=   GetEfmAccountdetailRecords(_641Value);

                if (_641Coll != null && _641Coll.Entities.Count > 0)
                {
                    decimal _641Budget = 0, _641Actual = 0;

                    foreach (var entity in _641Coll.Entities)
                    {
                        _641Budget += entity.Attributes.Contains("dfi_budgetamount") && entity.Attributes["dfi_budgetamount"] != null ? ((Money)entity.Attributes["dfi_budgetamount"]).Value : 0;
                        _641Actual += entity.Attributes.Contains("dfi_totalamount") && entity.Attributes["dfi_totalamount"] != null ? ((Money)entity.Attributes["dfi_totalamount"]).Value : 0;                        
                    }

                    _efmUpdate.Attributes["dfi_gastosdepersonal"] = new Money(_641Budget);
                    _efmUpdate.Attributes["dfi_gastosdepersonal_actual"] = new Money(_641Actual);
                    _efmUpdate.Attributes["dfi_gastosdepersonaldiferencia"] = new Money(_641Budget - Math.Abs(_641Actual));
                    CreateEfmSummaryRecords(_641SeqNum, _641text, _641Value, new Money(_641Budget), new Money(_641Actual));
                }

                //Money _641Budget = Efm.Attributes.Contains("dfi_gastosdepersonal") && Efm.Attributes["dfi_gastosdepersonal"] != null ? (Money)Efm.Attributes["dfi_gastosdepersonal"] : new Money(0);
                // Money _641Actual = Efm.Attributes.Contains("dfi_gastosdepersonal_actual") && Efm.Attributes["dfi_gastosdepersonal_actual"] != null ? (Money)Efm.Attributes["dfi_gastosdepersonal_actual"] : new Money(0);
                //CreateEfmSummaryRecords(_641SeqNum, _641text, _641Value, _641Budget, _641Actual);



                #endregion

                #region Efm Budget && Actual for "640-Sueldos y Salarios"
                int _640SeqNum = 2;
                string _640text = "640-Sueldos y Salarios";
                int _640Value = GetOptionsSetValueGivenText(service, "dfi_proveedortype", "dfi_accounttype", _640text);

                EntityCollection _640Coll = GetEfmAccountdetailRecords(_640Value);

                if (_640Coll != null && _640Coll.Entities.Count > 0)
                {
                    decimal _640Budget = 0, _640Actual = 0;
                    foreach (var entity in _640Coll.Entities)
                    {
                        _640Budget += entity.Attributes.Contains("dfi_budgetamount") && entity.Attributes["dfi_budgetamount"] != null ? ((Money)entity.Attributes["dfi_budgetamount"]).Value : 0;
                        _640Actual += entity.Attributes.Contains("dfi_totalamount") && entity.Attributes["dfi_totalamount"] != null ? ((Money)entity.Attributes["dfi_totalamount"]).Value : 0;
                    }

                    _efmUpdate.Attributes["dfi_sueldosysalarios"] = new Money(_640Budget);
                    _efmUpdate.Attributes["dfi_sueldosysalarios_actual"] = new Money(_640Actual);
                    _efmUpdate.Attributes["dfi_sueldosysalariosdiferencia"] = new Money(_640Budget - Math.Abs(_640Actual));
                    CreateEfmSummaryRecords(_640SeqNum, _640text, _640Value, new Money(_640Budget), new Money(_640Actual));
                }


                //Money _640Budget = Efm.Attributes.Contains("dfi_sueldosysalarios") && Efm.Attributes["dfi_sueldosysalarios"] != null ? (Money)Efm.Attributes["dfi_sueldosysalarios"] : new Money(0);
                //Money _640Actual = Efm.Attributes.Contains("dfi_sueldosysalarios_actual") && Efm.Attributes["dfi_sueldosysalarios_actual"] != null ? (Money)Efm.Attributes["dfi_sueldosysalarios_actual"] : new Money(0);
                //CreateEfmSummaryRecords(_640SeqNum, _640text, _640Value, _640Budget, _640Actual);


                #endregion

                #region Efm Budget && Actual for "642-Seguros socials a c�rrec de l'empresa"

                int _642SeqNum = 3;
                string _642text = "642-Seguros socials a c�rrec de l'empresa";
                int _642Value = GetOptionsSetValueGivenText(service, "dfi_proveedortype", "dfi_accounttype", _642text);
                EntityCollection _642Coll = GetEfmAccountdetailRecords(_642Value);
                if (_642Coll != null && _642Coll.Entities.Count > 0)
                {
                    decimal _642Budget = 0, _642Actual = 0;
                    foreach (var entity in _642Coll.Entities)
                    {
                        _642Budget += entity.Attributes.Contains("dfi_budgetamount") && entity.Attributes["dfi_budgetamount"] != null ? ((Money)entity.Attributes["dfi_budgetamount"]).Value : 0;
                        _642Actual += entity.Attributes.Contains("dfi_totalamount") && entity.Attributes["dfi_totalamount"] != null ? ((Money)entity.Attributes["dfi_totalamount"]).Value : 0;
                    }

                    _efmUpdate.Attributes["dfi_segurossocialsacarrecdeiempresa"] = new Money(_642Budget);
                    _efmUpdate.Attributes["dfi_segurossocialsacarrecdeiempresa_actual"] = new Money(_642Actual);
                    _efmUpdate.Attributes["dfi_segurossocialsacarrecdiferencia"] = new Money(_642Budget - Math.Abs(_642Actual));
                    CreateEfmSummaryRecords(_642SeqNum, _642text, _642Value, new Money(_642Budget), new Money(_642Actual));
                }



                ////Money _642Budget = Efm.Attributes.Contains("dfi_segurossocialsacarrecdeiempresa") && (Money)Efm.Attributes["dfi_segurossocialsacarrecdeiempresa"] != null ? (Money)Efm.Attributes["dfi_segurossocialsacarrecdeiempresa"] : new Money(0);
                ////Money _642Actual = Efm.Attributes.Contains("dfi_segurossocialsacarrecdeiempresa_actual") && (Money)Efm.Attributes["dfi_segurossocialsacarrecdeiempresa_actual"] != null ? (Money)Efm.Attributes["dfi_segurossocialsacarrecdeiempresa_actual"] : new Money(0);
                ////CreateEfmSummaryRecords(_642SeqNum, _642text, _642Value, _642Budget, _642Actual);


                #endregion

                #region Efm Budget && Actual for "62-Gastos Explotacion"
                int _62SeqNum = 4;
                string _62text = "62-Gastos Explotacion";
                int _62Value = GetOptionsSetValueGivenText(service, "dfi_proveedortype", "dfi_accounttype", _62text);

                EntityCollection _62Coll = GetEfmAccountdetailRecords(_62Value);
                if (_62Coll != null && _62Coll.Entities.Count > 0)
                {
                    decimal _62Budget = 0, _62Actual = 0;

                    foreach (var entity in _62Coll.Entities)
                    {
                        _62Budget += entity.Attributes.Contains("dfi_budgetamount") && entity.Attributes["dfi_budgetamount"] != null ? ((Money)entity.Attributes["dfi_budgetamount"]).Value : 0;
                        _62Actual += entity.Attributes.Contains("dfi_totalamount") && entity.Attributes["dfi_totalamount"] != null ? ((Money)entity.Attributes["dfi_totalamount"]).Value : 0;
                    }

                    _efmUpdate.Attributes["dfi_gastosexplotacion"] = new Money(_62Budget);
                    _efmUpdate.Attributes["dfi_gastosexplotacion_actual"] = new Money(_62Actual);
                    _efmUpdate.Attributes["dfi_gastosexplotaciondiferencia"] = new Money(_62Budget - Math.Abs(_62Actual));
                    CreateEfmSummaryRecords(_62SeqNum, _62text, _62Value, new Money(_62Budget), new Money(_62Actual));
                }

                //Money _62Budget = Efm.Attributes.Contains("dfi_gastosexplotacion") && (Money)Efm.Attributes["dfi_gastosexplotacion"] != null ? (Money)Efm.Attributes["dfi_gastosexplotacion"] : new Money(0);
                //Money _62Actual = Efm.Attributes.Contains("dfi_gastosexplotacion_actual") && (Money)Efm.Attributes["dfi_gastosexplotacion_actual"] != null ? (Money)Efm.Attributes["dfi_gastosexplotacion_actual"] : new Money(0);
                //CreateEfmSummaryRecords(_62SeqNum,_62text, _62Value, _62Budget, _62Actual);


                #endregion

                #region Efm Budget && Actual for "628-Suministros"
                int _628SeqNum = 5;
                string _628text = "628-Suministros";
                int _628Value = GetOptionsSetValueGivenText(service, "dfi_proveedortype", "dfi_accounttype", _628text);

                EntityCollection _628Coll = GetEfmAccountdetailRecords(_628Value);
                if (_628Coll != null && _628Coll.Entities.Count > 0)
                {
                    decimal _628Budget = 0, _628Actual = 0;

                    foreach (var entity in _628Coll.Entities)
                    {
                        _628Budget += entity.Attributes.Contains("dfi_budgetamount") && entity.Attributes["dfi_budgetamount"] != null ? ((Money)entity.Attributes["dfi_budgetamount"]).Value : 0;
                        _628Actual += entity.Attributes.Contains("dfi_totalamount") && entity.Attributes["dfi_totalamount"] != null ? ((Money)entity.Attributes["dfi_totalamount"]).Value : 0;
                    }

                    _efmUpdate.Attributes["dfi_suministros"] = new Money(_628Budget);
                    _efmUpdate.Attributes["dfi_suministros_actual"] = new Money(_628Actual);
                    _efmUpdate.Attributes["dfi_suministrosdiferencia"] = new Money(_628Budget - Math.Abs(_628Actual));
                    CreateEfmSummaryRecords(_628SeqNum, _628text, _628Value, new Money(_628Budget), new Money(_628Actual));
                }

                //Money _628Budget = Efm.Attributes.Contains("dfi_suministros") && (Money)Efm.Attributes["dfi_suministros"] != null ? (Money)Efm.Attributes["dfi_suministros"] : new Money(0);
                //Money _628Actual = Efm.Attributes.Contains("dfi_suministros_actual") && (Money)Efm.Attributes["dfi_suministros_actual"] != null ? (Money)Efm.Attributes["dfi_suministros_actual"] : new Money(0);
                //CreateEfmSummaryRecords(_628SeqNum,_628text, _628Value, _628Budget, _628Actual);


                #endregion

                #region Efm Budget && Actual for "629-Otros Servicios"
                int _629SeqNum = 6;
                string _629text = "629-Otros Servicios";
                int _629Value = GetOptionsSetValueGivenText(service, "dfi_proveedortype", "dfi_accounttype", _629text);
                EntityCollection _629Coll = GetEfmAccountdetailRecords(_629Value);
                if (_629Coll != null && _629Coll.Entities.Count > 0)
                {
                    decimal _629Budget = 0, _629Actual = 0;

                    foreach (var entity in _629Coll.Entities)
                    {
                        _629Budget += entity.Attributes.Contains("dfi_budgetamount") && entity.Attributes["dfi_budgetamount"] != null ? ((Money)entity.Attributes["dfi_budgetamount"]).Value : 0;
                        _629Actual += entity.Attributes.Contains("dfi_totalamount") && entity.Attributes["dfi_totalamount"] != null ? ((Money)entity.Attributes["dfi_totalamount"]).Value : 0;
                    }

                    _efmUpdate.Attributes["dfi_otrosservicios"] = new Money(_629Budget);
                    _efmUpdate.Attributes["dfi_otrosservicios_actual"] = new Money(_629Actual);
                    _efmUpdate.Attributes["dfi_otrosserviciosdiferencia"] = new Money(_629Budget - Math.Abs(_629Actual));
                    CreateEfmSummaryRecords(_629SeqNum, _629text, _629Value, new Money(_629Budget), new Money(_629Actual));
                }

                //Money _629Budget = Efm.Attributes.Contains("dfi_otrosservicios") && (Money)Efm.Attributes["dfi_otrosservicios"] != null ? (Money)Efm.Attributes["dfi_otrosservicios"] : new Money(0);
                //Money _629Actual = Efm.Attributes.Contains("dfi_otrosservicios_actual") && (Money)Efm.Attributes["dfi_otrosservicios_actual"] != null ? (Money)Efm.Attributes["dfi_otrosservicios_actual"] : new Money(0);
                //CreateEfmSummaryRecords(_629SeqNum,_629text, _629Value, _629Budget, _629Actual);

                #endregion

                service.Update(_efmUpdate);


            }
        }

        private void CreateEfmSummaryRecords(int SeqNum, string name, int value, Money EfmBudget, Money EfmActual)
        {
            Entity _efmSummary = new Entity("dfi_efmsummary");
            _efmSummary.Attributes["dfi_name"] = name;
            _efmSummary.Attributes["dfi_seqnumber"] = SeqNum;
            _efmSummary.Attributes["dfi_budget"] = EfmBudget;
            _efmSummary.Attributes["dfi_actual"] = EfmActual;
            _efmSummary.Attributes["dfi_difference"] = new Money(EfmBudget.Value - Math.Abs(EfmActual.Value));
            _efmSummary.Attributes["dfi_efmid"] = new EntityReference("new_efm", EfmId);

            Guid _efmSummaryId = service.Create(_efmSummary);

            if (_efmSummaryId != Guid.Empty)
            {
                EntityCollection efmAccount = GetEfmAccountdetailRecords(value);

                if (efmAccount != null && efmAccount.Entities.Count > 0)
                {
                    for (int i = 0; i < efmAccount.Entities.Count; i++)
                    {
                        var entity = efmAccount.Entities[i];
                        string AccSubTypeCode = ((AliasedValue)entity["ab.dfi_accountsubtypecode"]).Value.ToString();
                        EntityReference ProviderType = entity.Attributes.Contains("dfi_providertypeid") && entity.Attributes["dfi_providertypeid"] != null ? (EntityReference)entity.Attributes["dfi_providertypeid"] : null;
                        Money BudgetAmt = entity.Attributes.Contains("dfi_budgetamount") && entity.Attributes["dfi_budgetamount"] != null ? (Money)entity.Attributes["dfi_budgetamount"] : new Money(0);
                        Money ActualAmt = entity.Attributes.Contains("dfi_totalamount") && entity.Attributes["dfi_totalamount"] != null ? (Money)entity.Attributes["dfi_totalamount"] : new Money(0);
                        Money DiffAmt = entity.Attributes.Contains("dfi_difference") && entity.Attributes["dfi_difference"] != null ? (Money)entity.Attributes["dfi_difference"] : new Money(0);



                        Entity _efmAccountdetailSummary = new Entity("dfi_efmaccountdetailsummary");
                        _efmAccountdetailSummary.Attributes["dfi_efmaccountdetailid"] = new EntityReference("dfi_efmaccountdetail", entity.Id);
                        _efmAccountdetailSummary.Attributes["dfi_efmsummaryid"] = new EntityReference("dfi_efmsummary", _efmSummaryId); ;
                        _efmAccountdetailSummary.Attributes["dfi_accountsubtypecode"] = AccSubTypeCode;
                        _efmAccountdetailSummary.Attributes["dfi_name"] = AccSubTypeCode;
                        _efmAccountdetailSummary.Attributes["dfi_budgetamount"] = BudgetAmt;
                        _efmAccountdetailSummary.Attributes["dfi_totalamount"] = ActualAmt;
                        _efmAccountdetailSummary.Attributes["dfi_difference"] = new Money(BudgetAmt.Value - Math.Abs(ActualAmt.Value));
                        if (ProviderType != null)
                        {
                            _efmAccountdetailSummary.Attributes["dfi_providertypeid"] = ProviderType;
                        }

                        Guid _efmAccdetailSummaryId = service.Create(_efmAccountdetailSummary);

                        if (_efmAccdetailSummaryId != Guid.Empty)
                        {
                            EntityCollection proveedorColl = GetClientProveedorRecords(ProviderType.Id);
                            if (proveedorColl != null && proveedorColl.Entities.Count > 0)
                            {
                                foreach (var proveedor in proveedorColl.Entities)
                                {
                                    decimal TotalTransactionAmt = GetClientProveedorPayment(proveedor.Id);

                                    Entity _ClientProveedorSummary = new Entity("dfi_clientesproveedores");
                                    _ClientProveedorSummary.Attributes["dfi_efmsummaryid"] = new EntityReference("dfi_efmsummary", _efmSummaryId);
                                    _ClientProveedorSummary.Attributes["dfi_efmaccountdetailsummaryid"] = new EntityReference("dfi_efmaccountdetailsummary", _efmAccdetailSummaryId);
                                    _ClientProveedorSummary.Attributes["dfi_providerid"] = new EntityReference("account", proveedor.Id);
                                    _ClientProveedorSummary.Attributes["dfi_totalamount"] = new Money(TotalTransactionAmt);

                                    service.Create(_ClientProveedorSummary);
                                }
                            }
                        }
                    }
                }
            }
        }

       
        private EntityCollection GetEfmAccountdetailRecords(int value)
        {
            EntityCollection coll = null;
            string fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                          <entity name='dfi_efmaccountdetail'>
                            <attribute name='dfi_efmaccountdetailid' />
                            <attribute name='dfi_name' />                            
                            <attribute name='dfi_totalamount' />
                            <attribute name='dfi_difference' />
                            <attribute name='dfi_budgetamount' />
                            <attribute name='dfi_providertypeid' />
                            <order attribute='dfi_name' descending='false' />
                            <filter type='and'>
                              <condition attribute='dfi_efmid' operator='eq' uitype='new_efm' value='" + EfmId + @"' />
                            </filter>
                            <link-entity name='dfi_proveedortype' from='dfi_proveedortypeid' to='dfi_providertypeid' alias='ab'>
                            <attribute name='dfi_accountsubtypename' />
                            <attribute name = 'dfi_accountsubtypecode' />
                            <filter type='and'>
                                <condition attribute='dfi_accounttype' operator='eq' value='" + value + @"' />
                              </filter>
                            </link-entity>
                          </entity>
                        </fetch>";
            coll = service.RetrieveMultiple(new FetchExpression(fetch));
            return coll;
        }

       
        private EntityCollection GetClientProveedorRecords(Guid ProveedorTypeId)
        {
            EntityCollection _client = null;
            string fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                          <entity name='account'>
                            <attribute name='name' />
                            <attribute name='primarycontactid' />
                            <attribute name='accountid' />
                            <filter type='and'>
                              <condition attribute='dfi_proveedortypeid' operator='eq' value='" + ProveedorTypeId + @"' />
                            </filter>
                          </entity>
                        </fetch>";
            _client = service.RetrieveMultiple(new FetchExpression(fetch));
            return _client;
        }

        private decimal GetClientProveedorPayment(Guid ProveedorId)
        {
            decimal _totalTransactionamt = 0;
            string fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                              <entity name='dfi_clientproveedorpayment'>
                                <attribute name='dfi_clientproveedorpaymentid' />
                                <attribute name='dfi_name' />
                                <attribute name='createdon' />
                                <attribute name='dfi_transactionamount' />
                                <attribute name='dfi_transactiondate' />
                                <attribute name='dfi_proveedorid' />                                
                                <filter type='and'>
                                  <condition attribute='dfi_proveedorid' operator='eq' value='" + ProveedorId + @"' />
                                </filter>
                              </entity>
                            </fetch>";
            EntityCollection _coll = service.RetrieveMultiple(new FetchExpression(fetch));
            if (_coll != null && _coll.Entities.Count > 0)
            {

                foreach (var record in _coll.Entities)
                {
                    DateTime transactiondate = (DateTime)record.Attributes["dfi_transactiondate"];
                    if (transactiondate.Year.ToString() == EfmYear) {
                        decimal TrsAmt = ((Money)record.Attributes["dfi_transactionamount"]).Value;
                        _totalTransactionamt = _totalTransactionamt + TrsAmt;
                    }
                }

            }
            return _totalTransactionamt;
        }

        private void DeleteExistingEfmSummaryRecord(Guid efmId)
        {
            string fetch = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                              <entity name='dfi_efmsummary'>
                                <attribute name='dfi_efmsummaryid' />
                                <attribute name='dfi_name' />                                                            
                                <filter type='and'>
                                  <condition attribute='dfi_efmid' operator='eq' uitype='new_efm' value='" + efmId + @"' />
                                </filter>
                              </entity>
                            </fetch>";
            EntityCollection coll = service.RetrieveMultiple(new FetchExpression(fetch));
            if (coll != null && coll.Entities.Count > 0)
            {
                foreach (var record in coll.Entities)
                {
                    service.Delete("dfi_efmsummary", record.Id);
                }
            }
        }

        private int GetOptionsSetValueGivenText(IOrganizationService service, string entityName, string attributeName, string selectedValue)
        {
            try
            {
                RetrieveAttributeRequest retrieveAttributeRequest = new RetrieveAttributeRequest { EntityLogicalName = entityName, LogicalName = attributeName, RetrieveAsIfPublished = true };
                RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
                PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (PicklistAttributeMetadata)retrieveAttributeResponse.AttributeMetadata;
                OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
                int selectedOptionValue = 0;
                foreach (OptionMetadata oMD in optionList)
                {
                    if (oMD.Label.UserLocalizedLabel.Label == selectedValue)
                    {
                        selectedOptionValue = oMD.Value.Value;
                        break;
                    }
                }
                return selectedOptionValue;
            }
            catch (System.ServiceModel.FaultException ex1)
            {
                string strEr = ex1.InnerException.Data.ToString();
                return 0;
            }

        }
    }
}
