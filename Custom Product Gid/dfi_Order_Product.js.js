﻿function Opendialog() {    
    OpenWindowDialog("/WebResources/dfi_Custom_Product_Dialog", 1200,650, function (returnValue) {
        if (returnValue.length>0) {
            SetLookUp("productid", "product", returnValue[0], returnValue[1]);
            OrderProduct.Library.productid_OnChange();
        }
    })
}


function OpenModelDialog() {
    var URL = "/WebResources/dfi_Custom_Product_Dialog";
    var DialogOption = new Xrm.DialogOptions;
    DialogOption.width = 1200; DialogOption.height = 650;
    Xrm.Internal.openDialog(URL,
    DialogOption,
    null, null,
    CallbackFunction);
    function CallbackFunction(returnValue) {
        if (returnValue[0] != "") {
            SetLookUp("productid", "product", returnValue[0], returnValue[1]);
            OrderProduct.Library.productid_OnChange();
        }

    }
}


function OpendwindowDialogbox() {
    Xrm.Utility.openWebResource("dfi_Custom_Product_Dialog", null, 1200, 650);    
}


function SetLookUp(fieldName, fieldType, fieldId, value) {
    try {
        var object = new Array();
        object[0] = new Object();
        object[0].id = fieldId;
        object[0].name = value;
        object[0].entityType = fieldType;
        Xrm.Page.getAttribute(fieldName).setValue(object);
    }
    catch (e) {
        alert("Error in SetLookUp: fieldName = " + fieldName + " fieldType = " + fieldType + " fieldId = " + fieldId + " value = " + value + " error = " + e);
    }
}

/////////////////////////////////////////////////////////////////////////////////
//Open Lookup Window
//////////////////////////////////////////////////////////////////////////////////
if (typeof ($) == 'undefined') {
    debugger;
    $ = parent.$;
    jQuery = parent.jQuery;

}

$(document).ready(function () {
        isobjectCreated();
    
});

function isobjectCreated() {
    //Lookup Field Div productid_ledit 
        if ($("#productid_lookupTable").length > 0) {
        BindEventsToDropdown();
    }
    else
        setTimeout(isobjectCreated, 300);
}


function BindEventsToDropdown() {
    debugger;
    //class of search image control Lookup_RenderButton_td
    $("#productid_lookupTable").find(".Lookup_RenderButton_td").find('a').first().on('click', function () {
        if ($(".ms-crm-IL-Menu").length > 0) {
            $(".ms-crm-IL-Menu").hide();
            $(".ms-crm-modalDialog-shadow").hide()
        }
        Opendialog();      
    });
    $("#productid_lookupTable").find(".Lookup_RenderButton_td").find('img').first().on('click', function () {
        if ($(".ms-crm-IL-Menu").length > 0) {
            $(".ms-crm-IL-Menu").hide();
            $(".ms-crm-modalDialog-shadow").hide();
        }
        Opendialog();       
    });
}


function OpenWindowDialog(url, w, h, callback) {
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var win = window.open(url, "MyDialog", 'scrollbars=yes,menubar=0,toolbar=0, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
   // var win = window.open(url, "MyDialog", width, height, "menubar=0,toolbar=0");
   // win.resizeTo(1200, 650);
    var timer = setInterval(function () {
        if (win.closed) {
            clearInterval(timer);
            var returnValue = win.returnValue;
            callback(returnValue);
        }
    }, 500);
}



