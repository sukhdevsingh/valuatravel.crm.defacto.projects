﻿relatedAccounts = [];
var err = "";
var searchTextQuery = "";
var checkboxesQuery = "";
var checkboxServicioQuery = "";
var checkboxCategoriaQuery = "";
var checkboxtipdeQuery = "";
var currentpage = 1;
var blankRow = false;

var datequery = "";


$(document).ready(function () {
    try{
        $(".loader").show();
        debugger;
        $('#Tipodechk').empty();
        $(".Tipodelabel").hide();
        ProductFetchRequest();
    
    
        //Collapse div
        $("#servicioCollapse").click(function () {
            $("#serviciochk").slideToggle();
        });
        $("#TipodeCollapse").click(function () {
            $("#Tipodechk").slideToggle();
        });
        $("#categoriaCollapse").click(function () {
            $("#categoriachk").slideToggle();
        });
        $("#SearchCollapse").click(function () {
            $("#searchBox").slideToggle();
        });


        $(".searchlabel,.serviciolabel,.Tipodelabel,.categorialabel,#serviciochk,#Tipodechk,#categoriachk").hover(

               function () {
                   $(this).css({ "color": "black" });
               },

               function () {
                   $(this).css({ "color": "black" });
               }
            );

        $('.ChkservicioClass').click(function () {
            $(".loader").show();

            if ($(this).is(":checked")) {
                $(".Tipodelabel").show();
                $("#Tipodechk").height("100px")
                OnChangeServicio($(this).val());
            }

            else {
                $('#Tipodechk').empty();
                var values = $('input:checkbox:checked.ChkservicioClass').map(function () {
                    return this.value;
                }).get();

                if (values.length > 0) {
                    $(".Tipodelabel").show();
                    $("#Tipodechk").height("100px")
                    for (var i = 0; i < values.length; i++) {
                        OnChangeServicio(values[i]);
                    }
                }
                else {
                    $(".Tipodelabel").hide();
                    $('#Tipodechk').empty();
                    $("#Tipodechk").height("0px")
                }
            }

            debugger;

            var multiServicio = [];
            var Servicio = [];
            checkboxServicioQuery = "";

            $(".ChkservicioClass:checked").each(function () {
                Servicio.push($(this).val());
                multiServicio.push("<value>" + $(this).val() + "</value>");
            });

            if (multiServicio.length == 0) {
                checkboxServicioQuery = "";
            }
            else if (multiServicio.length == 1) {
                checkboxServicioQuery = "<condition attribute='new_servicio' operator='eq' value='" + Servicio.toString() + "' />";
            }
     
            else if (multiServicio.length > 1) {
                checkboxServicioQuery = "<condition attribute='new_servicio' operator='in' >" + multiServicio.toString().replace(',', '') + "</condition>";

            }

       
            ProductFetchRequest();
            $(".loader").fadeOut("slow");

        });

  
        $('.ChkcatergriaClass').click(function () {
            $(".loader").show();
            var multicatergria = [];
            var catergria = [];
            checkboxCategoriaQuery = "";

            $(".ChkcatergriaClass:checked").each(function () {
                catergria.push($(this).val());
                multicatergria.push("<value>" + $(this).val() + "</value>");

            });

            if (multicatergria.length == 0) {
                checkboxCategoriaQuery = "";
            }

            else if (multicatergria.length == 1) {
                checkboxCategoriaQuery = "<condition attribute='new_categoria' operator='eq' value='" + catergria.toString() + "' />";
            }
     
            else if (multicatergria.length > 1) {
                checkboxCategoriaQuery = "<condition attribute='new_categoria' operator='in' >" + multicatergria.toString().replace(',', '') + "</condition>";
           
            }      

       
            ProductFetchRequest();
            $(".loader").fadeOut("slow");
        });
        $(".loader").fadeOut("slow");
    }

    catch(err){    
        $(".loader").fadeOut("slow");
    }
});




function OnChangeServicio(value) {
    if (value == "100000000") {
        $('#Tipodechk').append('<input class="ChkTipodeClass" type="checkbox" id="100000000" name="AEREO" value="100000000" />AEREO<br /><input class="ChkTipodeClass" type="checkbox" id="100000001" name="CARRETERA" value="100000001" />CARRETERA<br /><input class="ChkTipodeClass" type="checkbox" id="100000002" name="FERROVIARIO" value="100000002" />FERROVIARIO<br /><input class="ChkTipodeClass" type="checkbox" id="100000003" name="MAR" value="100000003" />MAR<br /><input class="ChkTipodeClass" type="checkbox" id="100000090" name="METROPOLITAN" value="100000090" />METROPOLITAN<br />');
    }
    else if (value == "100000001") {
        $('#Tipodechk').append('<input class="ChkTipodeClass" type="checkbox" id="100000005" name="HOMESTAY" value="100000005" />HOMESTAY<br /><input class="ChkTipodeClass" type="checkbox" id="100000100" name="HOTEL" value="100000100" />HOTEL<br /><input class="ChkTipodeClass" type="checkbox" id="100000101" name="ALBERGUE" value="100000101" />ALBERGUE<br /><input class="ChkTipodeClass" type="checkbox" id="100000102" name="HOSTAL" value="100000102" />HOSTAL<br /><input class="ChkTipodeClass" type="checkbox" id="100000103" name="CASA RURAL" value="100000103" />CASA RURAL<br /><input class="ChkTipodeClass" type="checkbox" id="100000104" name="CAMPING" value="100000104" />CAMPING<br />');
    }
    else if (value == "100000002") {
        $('#Tipodechk').append('<input class="ChkTipodeClass" type="checkbox" id="100000200" name="SEGURO" value="100000200" />SEGURO<br />');
    }
    else if (value == "100000003") {
        $('#Tipodechk').append('<input class="ChkTipodeClass" type="checkbox" id="100000300" name="RESTAURANTE" value="100000300" />RESTAURANTE<br /><input class="ChkTipodeClass" type="checkbox" id="100000301" name="PICNIC" value="100000301" />PICNIC<br />');
    }
    else if (value == "100000004") {
        $('#Tipodechk').append('<input class="ChkTipodeClass" type="checkbox" id="100000007" name="DEPORTES" value="100000007" />DEPORTES<br /><input class="ChkTipodeClass" type="checkbox" id="100000400" name="MUSEO" value="100000400" />MUSEO<br /><input class="ChkTipodeClass" type="checkbox" id="100000401" name="MONUMENTO" value="100000401" />MONUMENTO<br /><input class="ChkTipodeClass" type="checkbox" id="100000402" name="RUINAS" value="100000402" />RUINAS<br /><input class="ChkTipodeClass" type="checkbox" id="100000403" name="FUNDACIONES" value="100000403" />FUNDACIONES<br /><input class="ChkTipodeClass" type="checkbox" id="100000404" name="PARQUES TEMÁTICOS" value="100000404" />PARQUES TEMÁTICOS<br /><input class="ChkTipodeClass" type="checkbox" id="100000410" name="COLISEO" value="100000410" />COLISEO<br /><input class="ChkTipodeClass" type="checkbox" id="100000411" name="TORRE EIFFEL" value="100000411" />TORRE EIFFEL<br/><input class="ChkTipodeClass" type="checkbox" id="100000413" name="ACONTECIMIЕNTO" value="100000413" />ACONTECIMIЕNTO<br/><input class="ChkTipodeClass" type="checkbox" id="100000414" name="GASTRONOMÍA" value="100000414" />GASTRONOMÍAЕNTO<br/><input class="ChkTipodeClass" type="checkbox" id="100000415" name="VISITA CULTURAL" value="100000415" />VISITA CULTURAL<br/><input class="ChkTipodeClass" type="checkbox" id="100000416" name="NORIA" value="100000416" />NORIA<br/><input class="ChkTipodeClass" type="checkbox" id="100000417" name="ORGANISMOS GUBERNAMENTALES" value="100000417" />ORGANISMOS GUBERNAMENTALES<br/><input class="ChkTipodeClass" type="checkbox" id="100000418" name="CASTILLOS" value="100000418" />CASTILLOS<br/><input class="ChkTipodeClass" type="checkbox" id="100000419" name="RELIGIOSO" value="100000419" />RELIGIOSO<br/><input class="ChkTipodeClass" type="checkbox" id="100000421" name="RELIGIOSO" value="100000421" />RELIGIOSO<br/><input class="ChkTipodeClass" type="checkbox" id="100000422" name="DEPORTE MAR" value="100000422" />DEPORTE MAR<br/><input class="ChkTipodeClass" type="checkbox" id="100000423" name="DEPORTE MONTAÑA" value="100000423" />DEPORTE MONTAÑA<br/><input class="ChkTipodeClass" type="checkbox" id="100000424" name="DEPORTE MONTAÑA" value="100000424" />DEPORTE NIEVE<br/><input class="ChkTipodeClass" type="checkbox" id="100000424" name="DEPORTE NIEVE" value="100000424" />DEPORTE NIEVE<br/><input class="ChkTipodeClass" type="checkbox" id="100000425" name="BARCO" value="100000425" />BARCO<br/><input class="ChkTipodeClass" type="checkbox" id="100000426" name="INDUSTRIAL" value="100000426" />INDUSTRIAL<br/><input class="ChkTipodeClass" type="checkbox" id="100000427" name="NATURALEZA" value="100000427" />NATURALEZA<br/><input class="ChkTipodeClass" type="checkbox" id="100000428" name="SITIO ARQUEOLÓGICO" value="100000428" />SITIO ARQUEOLÓGICO<br/><input class="ChkTipodeClass" type="checkbox" id="100000429" name="ESPECTÁCULOS" value="100000429" />ESPECTÁCULOS<br/><input class="ChkTipodeClass" type="checkbox" id="100000430" name="CURSO" value="100000430" />CURSO<br/>');
    }
    else if (value == "100000005") {
        $('#Tipodechk').append('<input class="ChkTipodeClass" type="checkbox" id="100000500" name="GUÍA CORREO" value="100000500" />GUÍA CORREO<br /><input class="ChkTipodeClass" type="checkbox" id="100000501" name="GUÍA OFICIAL" value="100000501" />GUÍA OFICIAL<br /><input class="ChkTipodeClass" type="checkbox" id="100000502" name="ASISTENCIA" value="100000502" />ASISTENCIA<br />');
    }
    else if (value == "100000006") {
        $('#Tipodechk').append('<input class="ChkTipodeClass" type="checkbox" id="100000006" name="TASA TURISTICA" value="100000006" />TASA TURISTICA<br /><input class="ChkTipodeClass" type="checkbox" id="100000600" name="PROMOCIÓN" value="100000600" />PROMOCIÓN<br /><input class="ChkTipodeClass" type="checkbox" id="100000601" name="MARKETING FEE" value="100000601" />MARKETING FEE<br /><input class="ChkTipodeClass" type="checkbox" id="100000602" name="SERVICE FEE" value="100000602" />SERVICE FEE<br /><input class="ChkTipodeClass" type="checkbox" id="100000603" name="GRATUÏDADES" value="100000603" />GRATUÏDADES<br />');
    }

    $(".ChkTipodeClass").click(function () {
        var multiTipode = [];
        var Tipode = [];
        checkboxtipdeQuery = "";

        $(".ChkTipodeClass:checked").each(function () {
            Tipode.push($(this).val());
            multiTipode.push("<value>" + $(this).val() + "</value>");

        });

        if (multiTipode.length == 0) {
            checkboxtipdeQuery = "";

        }

        else if (multiTipode.length == 1) {
            checkboxtipdeQuery = "";
        }
        else if (multiTipode.length > 1) {
            checkboxtipdeQuery = "<condition attribute='new_tipologia' operator='in' >" + multiTipode.toString().replace(',', '') + "</condition>";
            }
       

       
            ProductFetchRequest();
    });
}




function resetFilters() {
    $(".loader").show();
    searchTextQuery = "";
    checkboxServicioQuery = "";
    checkboxtipdeQuery = "";
    checkboxCategoriaQuery = "";  
    $("#txtFilterDate").val("");
    $("#txtProvider").val("");
    $("#txtproduct").val("");
    $("#txtCity").val("");

    $('#serviciochk input:checked').each(function () {
        $(this).attr('checked', false);
    });
    $('#Tipodechk input:checked').each(function () {
        $(this).attr('checked', false);
    });
    $('#categoriachk input:checked').each(function () {
        $(this).attr('checked', false);
    });

    ProductFetchRequest();

    $(".loader").fadeOut("slow");
}



function ProductFetchRequest() {
    
    try {
        $(".loader").show();
        blankRow = true;
        $("#mainGridtable tr:gt(0)").remove();
        var fetchXml = "<fetch count='200' page='1' version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                      "<entity name='product'>" +
                        "<attribute name='name' />" +
                        "<attribute name='productid' />" +
                        "<attribute name='description' />" +
                        "<attribute name='statecode' />" +
                        "<attribute name='new_tipologia' />" +
                        "<attribute name='new_nombrecomercial' />" +
                         "<attribute name='new_servicio' />" +
                         "<attribute name='new_tipologia' />" +
                         "<attribute name='new_fechainicioservicio' />" +
                         "<attribute name='new_fechafindeservicio' />" +
                         "<attribute name='new_categoria' />" +
                         "<attribute name='price' />" +
                         "<order attribute='name' descending='false' />" +
                        "<filter type='and'>" +
                          "<condition attribute='statecode' operator='eq' value='0' />";



        if (checkboxServicioQuery != "") {
            fetchXml += checkboxServicioQuery;
        }

        if (checkboxtipdeQuery != "") {
            fetchXml += checkboxtipdeQuery;
        }

        if (checkboxCategoriaQuery != "") {
            fetchXml += checkboxCategoriaQuery;
        }

        if (searchTextQuery != "") {
            fetchXml += searchTextQuery;
        }

        var endfetchXml = "</filter>" +
                            "</entity>" +
                            "</fetch>";

        fetchXml += endfetchXml;



        var results = XrmServiceToolkit.Soap.Fetch(fetchXml);
        if (results.length > 0) {

            var _count = 1;
            if (blankRow) {
                $('#mainGridtableClone tr:last').addClass('tablerowcss');
                $('#mainGridtable').append($("#mainGridtableClone tr:last").clone());
                $("#mainGridtable tr:last td:nth-child(1)").html("");
                $("#mainGridtable tr:last td:nth-child(2)").html("");
                $("#mainGridtable tr:last td:nth-child(3)").html("");
                $("#mainGridtable tr:last td:nth-child(4)").html("");
                $("#mainGridtable tr:last td:nth-child(5)").html("");
                $("#mainGridtable tr:last td:nth-child(6)").html("");
                $("#mainGridtable tr:last td:nth-child(7)").html("");
                $("#mainGridtable tr:last td:nth-child(8)").html("");
                $("#mainGridtable tr:last").css('height', '10px');
                blankRow = false;
            }

            for (var i = 0; i < (results.length) ; i++) {

                if (results[i].id != null) {
                    $('#mainGridtableClone tr:last').addClass('tablerowcss');
                    $('#mainGridtable').append($("#mainGridtableClone tr:last").clone());
                    $("#mainGridtable tr:last td:nth-child(1)").html(results[i].id);
                    $("#mainGridtable tr:last td:nth-child(2)").html(results[i].attributes["name"] != undefined ? results[i].attributes["name"].value : "");
                    $("#mainGridtable tr:last td:nth-child(3)").html(results[i].attributes["new_nombrecomercial"] != undefined ? results[i].attributes["new_nombrecomercial"].name : "");
                    $("#mainGridtable tr:last td:nth-child(4)").html(results[i].attributes["new_servicio"] != undefined ? results[i].attributes["new_servicio"].formattedValue : "");
                    $("#mainGridtable tr:last td:nth-child(5)").html(results[i].attributes["new_tipologia"] != undefined ? results[i].attributes["new_tipologia"].formattedValue : "");
                    $("#mainGridtable tr:last td:nth-child(6)").html(results[i].attributes["new_categoria"] != undefined ? results[i].attributes["new_categoria"].formattedValue : "");
                    $("#mainGridtable tr:last td:nth-child(7)").html(results[i].attributes["new_fechainicioservicio"] != undefined ? results[i].attributes["new_fechainicioservicio"].value.toLocaleDateString() : "");
                    $("#mainGridtable tr:last td:nth-child(8)").html(results[i].attributes["new_fechafindeservicio"] != undefined ? results[i].attributes["new_fechafindeservicio"].value.toLocaleDateString() : "");
                    $("#mainGridtable tr:last td:nth-child(9)").html(results[i].attributes["price"] != undefined ? results[i].attributes["price"].formattedValue : "");
                    $("#mainGridtable tr:last").attr('ondblclick', 'OpenCRMRecord(event)');
                    $("#mainGridtable tr:last").attr('onclick', 'GetIdOfRow(event)');
                }
                _count++;
            }

            var _event = jQuery.Event('#mainGridtable tr:nth-child(2)');

            GetIdOfRow($('#mainGridtable tr:nth-child(2)'));

            $(".loader").fadeOut("slow");
        }
    }
    catch (err) {
        $(".loader").fadeOut("slow");
    }
}

function retreive(pagenumber) {
    blankRow = true;
    currentpage = pagenumber;
   
    var skiprecords = (pagenumber - 1) * 100;

    $("#mainGridtable tr:gt(0)").remove();
    var selectquery = "?$select=Description,Name,ProductId,ProductNumber,ProductTypeCode,Price,new_Ciudad,new_DescriptioninCatalan,new_NOMBRECOMERCIAL,new_CATEGORIA,new_Servicio,new_Tipologia,new_FechaInicioServicio,new_FECHAFINDESERVICIO&$orderby=Name asc&$top=100&$skip=" + skiprecords + "&$filter=StateCode/Value eq 0";

    if (searchTextQuery != "")
        selectquery = selectquery + searchTextQuery;

    if (checkboxServicioQuery != "") {
        if (checkboxServicioQuery.indexOf(" or ") == 0) {
            checkboxServicioQuery = checkboxServicioQuery.substring(3);
        }

        selectquery = selectquery + " and " + checkboxServicioQuery;

    }

    if (checkboxtipdeQuery != "") {
        if (checkboxtipdeQuery.indexOf(" or ") == 0) {
            checkboxtipdeQuery = checkboxtipdeQuery.substring(3);
        }

        selectquery = selectquery + " and " + checkboxtipdeQuery;


    }


    if (checkboxCategoriaQuery != "") {
        if (checkboxCategoriaQuery.indexOf(" or ") == 0) {
            checkboxCategoriaQuery = checkboxCategoriaQuery.substring(3);
        }

        selectquery = selectquery + " and " + checkboxCategoriaQuery;
    }



    SDK.REST.retrieveMultipleRecordsSync("Product", selectquery, RetriveOrderData, ErrorMsg, Complete);
}

function ErrorMsg() { }
function Complete() { }

function RetriveOrderData(results, textStatus, XmlHttpRequest) {
    debugger;
    var _count = 1;
    if (blankRow) {
        $('#mainGridtableClone tr:last').addClass('tablerowcss');
        $('#mainGridtable').append($("#mainGridtableClone tr:last").clone());
        $("#mainGridtable tr:last td:nth-child(1)").html("");
        $("#mainGridtable tr:last td:nth-child(2)").html("");
        $("#mainGridtable tr:last td:nth-child(3)").html("");
        $("#mainGridtable tr:last td:nth-child(4)").html("");
        $("#mainGridtable tr:last td:nth-child(5)").html("");
        $("#mainGridtable tr:last td:nth-child(6)").html("");
        $("#mainGridtable tr:last td:nth-child(7)").html("");
        $("#mainGridtable tr:last td:nth-child(8)").html("");
        $("#mainGridtable tr:last").css('height', '10px');
        blankRow = false;
    }
    for (var i = 0; i < (results.length) ; i++) {

        if (results[i].ProductId != null) {
            $('#mainGridtableClone tr:last').addClass('tablerowcss');
            $('#mainGridtable').append($("#mainGridtableClone tr:last").clone());
            $("#mainGridtable tr:last td:nth-child(1)").html(results[i].ProductId);
            $("#mainGridtable tr:last td:nth-child(2)").html(results[i].Name != null ? results[i].Name : "");
            $("#mainGridtable tr:last td:nth-child(3)").html(results[i].new_NOMBRECOMERCIAL.Name != null ? results[i].new_NOMBRECOMERCIAL.Name : "");
            $("#mainGridtable tr:last td:nth-child(4)").html(results[i].new_Servicio.Value != null ? GetOptionSetText_Servicio(results[i].new_Servicio.Value) : "");
            $("#mainGridtable tr:last td:nth-child(5)").html(results[i].new_Tipologia.Value != null ? GetOptionSetText_TipodeServicio(results[i].new_Tipologia.Value) : "");
            $("#mainGridtable tr:last td:nth-child(6)").html(results[i].new_CATEGORIA.Value != null ? GetOptionSetText_Categoria(results[i].new_CATEGORIA.Value) : "");
            $("#mainGridtable tr:last td:nth-child(7)").html(results[i].new_FechaInicioServicio != null ? results[i].new_FechaInicioServicio.toLocaleDateString() : "");
            $("#mainGridtable tr:last td:nth-child(8)").html(results[i].new_FECHAFINDESERVICIO != null ? results[i].new_FECHAFINDESERVICIO.toLocaleDateString() : "");
            $("#mainGridtable tr:last td:nth-child(9)").html(results[i].Price.Value != null ? results[i].Price.Value : "");
            $("#mainGridtable tr:last").attr('ondblclick', 'OpenCRMRecord(event)');
            $("#mainGridtable tr:last").attr('onclick', 'GetIdOfRow(event)');
        }
        _count++;
    }

    var _event = jQuery.Event('#mainGridtable tr:nth-child(2)');

    GetIdOfRow($('#mainGridtable tr:nth-child(2)'));

    if (results.length < 100) {
        setPagination(true);
    }
    else {
        setPagination(false);
    }
}

var parameters = "";


function OpenCRMRecord(e) {
    debugger;
    if (e.currentTarget != null) {
        if (e.currentTarget.childNodes != null) {
            if (e.currentTarget.childNodes[1].innerText != "" && e.currentTarget.childNodes[1].innerText != null) {
                var _salesorderdetailId = e.currentTarget.childNodes[1].innerText;
                var _productName = e.currentTarget.childNodes[2].nextElementSibling.innerText
                parameters = new Array(_salesorderdetailId, _productName);
                //Mscrm.Utilities.setReturnValue(parameters);
                window.returnValue = parameters;
            }
            try {
                closeWindow(true);//Close the dialog box
            }
            catch (e) { }

        }
    }
}



var globalProductId = "";
var productname = "";
var temp = "";
function GetIdOfRowold(e) {

    if (temp == "") {
        if (e.currentTarget != null) {
            if (e.currentTarget.childNodes != null) {
                if (e.currentTarget.childNodes[1].innerText != "" && e.currentTarget.childNodes[1].innerText != null) {
                    temp = e.currentTarget;
                    e.currentTarget.style.backgroundColor = "#cce6ff";
                    globalProductId = e.currentTarget.childNodes[1].innerText;

                }

            }
        }
    }
    else {
        if (e.currentTarget != null) {
            if (e.currentTarget.childNodes != null) {
                if (e.currentTarget.childNodes[1].innerText != "" && e.currentTarget.childNodes[1].innerText != null) {
                    temp.style.backgroundColor = "";
                    e.currentTarget.style.backgroundColor = "#cce6ff";
                    temp = e.currentTarget;
                    globalProductId = e.currentTarget.childNodes[1].innerText;
                }

            }
        }
    }

}



function GetIdOfRow(e) {

    debugger;
    if (e.currentTarget != null) {
        if (e.currentTarget.childNodes != null) {
            if (e.currentTarget.childNodes[1].innerText != "" && e.currentTarget.childNodes[1].innerText != null) {
                var _salesorderdetailId = e.currentTarget.childNodes[1].innerText;
                var _productName = e.currentTarget.childNodes[2].nextElementSibling.innerText
                parameters = new Array(_salesorderdetailId, _productName);
                window.returnValue = parameters;
                //Mscrm.Utilities.setReturnValue(parameters);
            }
            try {

            }
            catch (e) { }

        }
    }

    if (temp == "") {
        if (e.find("td:first").text() != null) {
            if (e.find("td:first").text() != null) {
                if (e.find("td:first").text() != "" && e.find("td:first").text() != null) {
                    temp = e;
                    //e.find("td:first").style.backgroundColor = "#cce6ff";
                    e.css("background-color", "#cce6ff");
                    globalProductId = e.find("td:first").text();

                }

            }
        }
    }
    else {
        if (e.find == undefined) {
            if (e.currentTarget != null) {
                if (e.currentTarget.childNodes != null) {
                    if (e.currentTarget.childNodes[1].innerText != "" && e.currentTarget.childNodes[1].innerText != null) {
                        if (temp.find == undefined) {
                            temp.style.backgroundColor = "";
                            e.currentTarget.style.backgroundColor = "#cce6ff";
                            temp = e.currentTarget;
                            globalProductId = e.currentTarget.childNodes[1].innerText;
                        }
                        else {
                            temp.css("background-color", "")
                            //e.css("background-color", "#cce6ff");
                            e.currentTarget.style.backgroundColor = "#cce6ff";
                            temp = e.currentTarget;
                            globalProductId = e.currentTarget.childNodes[1].innerText;
                        }
                    }

                }
            }

        }
        else if (e.find("td:first").text() != null) {
            if (e.find("td:first").text() != null) {
                if (e.find("td:first").text() != "" && e.find("td:first").text() != null) {
                    temp.css("background-color", "")
                    // e.currentTarget.style.backgroundColor = "#cce6ff";
                    e.css("background-color", "#cce6ff");
                    temp = e;
                    globalProductId = e.find("td:first").text();
                }

            }
        }
    }

}

function okClick() {
    debugger;
    if (parameters != "") {
        Mscrm.Utilities.setReturnValue(parameters);

        try {
            closeWindow(true);//Close the dialog box
        }
        catch (e) { }
    }
}


function errorCallback() {
    //alert("Error Occured");
}



function OpenProviderLookup() {
    var serverUrl = window.parent.Xrm.Page.context.getClientUrl();
    var DialogOptions = new Xrm.DialogOptions();
    DialogOptions.width = 600;
    DialogOptions.height = 600;

    var url = serverUrl + "/_controls/lookup/lookupsingle.aspx?class=null&objecttypes=1&browse=0&ShowNewButton=0&ShowPropButton=1&DefaultType=0";
    Xrm.Internal.openDialog(url, DialogOptions, null, null, ProviderCallbackFunction);

}

function ProviderCallbackFunction(event) {
    var id = event.items[0].id
    var name = event.items[0].name;
    document.getElementById("txtProvider").value = name;
    document.getElementById("txtProvider").name = id;
}

function OpenCityLookup() {
    var serverUrl = window.parent.Xrm.Page.context.getClientUrl();
    var DialogOptions = new Xrm.DialogOptions();
    DialogOptions.width = 600;
    DialogOptions.height = 600;
    var url = serverUrl + "/_controls/lookup/lookupsingle.aspx?class=null&objecttypes=10007&browse=0&ShowNewButton=0&ShowPropButton=1&DefaultType=0";
    Xrm.Internal.openDialog(url, DialogOptions, null, null, CityCallbackFunction);
}

function CityCallbackFunction(event) {
    var id = event.items[0].id
    var name = event.items[0].name;
    document.getElementById("txtCity").value = name;
    document.getElementById("txtCity").name = id;
}



function GetOptionSetText_Servicio(value) {

    switch (value) {
        case 100000000:
            text = "TRANSPORTE";
            break;
        case 100000001:
            text = "ALOJAMIENTO";
            break;
        case 100000002:
            text = "SEGUROS";
            break;
        case 100000003:
            text = "RESTAURACIÓN";
            break;
        case 100000004:
            text = "VISITAS";
            break;
        case 100000005:
            text = "ASISTENCIA";
            break;
        case 100000006:
            text = "SERVICIO VENTA";
            break;
        case 100000007:
            text = "OTRO";
            break;
        case 100000008:
            text = "EXCURSIONES";
            break;
        case 100000009:
            text = "PAQUETE";
            break;
    }
    return text;


}

function GetOptionSetText_TipodeServicio(value) {
    var text = "";
    switch (value) {
        case 100000413:
            text = "ACONTECIMIЕNTO";
            break;
        case 100000000:
            text = "AEREO";
            break;
        case 100000101:
            text = "ALBERGUE";
            break;
        case 100000502:
            text = "ASISTENCIA";
            break;
        case 100000425:
            text = "BARCO";
            break;
        case 100000104:
            text = "CAMPING";
            break;
        case 100000001:
            text = "CARRETERA";
            break;
        case 100000103:
            text = "CASA RURAL";
            break;
        case 100000418:
            text = "CASTILLOS";
            break;
        case 100000410:
            text = "COLISEO";
            break;
        case 100000430:
            text = "CURSO";
            break;
        case 100000422:
            text = "DEPORTE MAR";
            break;
        case 100000423:
            text = "DEPORTE MONTAÑA";
            break;
        case 100000424:
            text = "DEPORTE NIEVE";
            break;
        case 100000007:
            text = "DEPORTES";
            break;
        case 100000429:
            text = "ESPECTÁCULOS";
            break;
        case 100000002:
            text = "FERROVIARIO";
            break;
        case 100000403:
            text = "FUNDACIONES";
            break;
        case 100000414:
            text = "GASTRONOMÍA";
            break;
        case 100000603:
            text = "GRATUÏDADES";
            break;
        case 100000500:
            text = "GUÍA CORREO";
            break;
        case 100000501:
            text = "GUÍA OFICIAL";
            break;
        case 100000005:
            text = "HOMESTAY    ";
            break;
        case 100000102:
            text = "HOSTAL";
            break;
        case 100000100:
            text = "HOTEL";
            break;
        case 100000900:
            text = "IDIOMÁTICO GRUPOS";
            break;
        case 100000901:
            text = "IDIOMÁTICO INDIVIDUAL";
            break;
        case 100000426:
            text = "INDUSTRIAL";
            break;
        case 100000905:
            text = "MAJORISTA";
            break;
        case 100000003:
            text = "MAR";
            break;
        case 100000601:
            text = "MARKETING FEE";
            break;
        case 100000090:
            text = "METROPOLITAN";
            break;
        case 100000401:
            text = "MONUMENTO";
            break;
        case 100000400:
            text = "MUSEO";
            break;
        case 100000427:
            text = "NATURALEZA";
            break;
        case 100000416:
            text = "NORIA";
            break;
        case 100000417:
            text = "ORGANISMOS GUBERNAMENTALES";
            break;
        case 100000004:
            text = "OTRO";
            break;
        case 100000404:
            text = "PARQUES TEMÁTICOS";
            break;
        case 100000301:
            text = "PICNIC";

        case 100000600:
            text = "PROMOCIÓN";
            break;
        case 100000419:
            text = "RELIGIOSO";
            break;
        case 100000300:
            text = "RESTAURANTE";
            break;
        case 100000402:
            text = "RUINAS";
            break;
        case 100000200:
            text = "SEGURO";
            break;
        case 100000904:
            text = "SEMANA AZUL";
            break;
        case 100000902:
            text = "SEMANA BLANCA";
            break;
        case 100000903:
            text = "SEMANA VERDE";
            break;
        case 100000602:
            text = "SERVICE FEE";
            break;
        case 100000428:
            text = "SITIO ARQUEOLÓGICO";
            break;

        case 100000006:
            text = "TASA TURISTICA";
            break
        case 100000421:
            text = "TERMALISMO";
            break
        case 100000411:
            text = "TORRE EIFFEL";
            break
        case 100000415:
            text = "VISITA CULTURAL";
            break
    }
    return text;


}

function GetOptionSetText_Categoria(value) {
    switch (value) {
        case 100000000:
            text = "1";
            break;
        case 100000001:
            text = "2";
            break;
        case 100000002:
            text = "3";
            break;
        case 100000003:
            text = "4";
            break;
        case 100000004:
            text = "5";
            break;
        case 100000005:
            text = "Basica";
            break;
        case 100000006:
            text = "Superior";
            break;
    }
    return text;
}

function searchTextClicked() {
    $(".loader").show();
    var QueryTemp = "";
    var filterdate = $('#txtFilterDate').val();
    var searchProvider = $('#txtProvider').val();
    var searchProviderId = $('#txtProvider').attr('name').replace("{", "").replace("}", "");
    var searchProduct = $("#txtproduct").val();
    var searchCity = $('#txtCity').val();
    var searchCityId = $('#txtCity').attr('name').replace("{", "").replace("}", "");


    if (filterdate == "" && searchProvider == "" && searchProduct == "" && searchCity == "") {
        searchTextQuery = "";
        ProductFetchRequest();
    }

    if (filterdate != "") {
        var date = new Date(filterdate);
        var formateDate = date.format("yyyy-MM-dd")
        QueryTemp = "<condition attribute='new_fechafindeservicio' operator='on-or-after' value='" + formateDate + "' />";

    }

    if (searchProvider != "") {      
        QueryTemp += "<condition attribute='new_nombrecomercialname' operator='like' value='%" + searchProvider + "%' />";
    }

    if (searchProduct != "") {
        QueryTemp += "<condition attribute='name' operator='like' value='%" + searchProduct + "%' />";
    }

    if (searchCity != "") {
        QueryTemp += "<condition attribute='new_ciudadname' operator='like' value='%" + searchCity + "%' />";
    }

    if (QueryTemp != "") {
        searchTextQuery = QueryTemp;
        ProductFetchRequest();
    }

    $(".loader").fadeOut("slow");
}

