﻿
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Data;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Crm.Sdk.Messages;
using System.Net;
using Microsoft.Xrm.Tooling.Connector;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using System.Text.RegularExpressions;

namespace ValuaTravel_Pedidos_CRM
{

    public partial class SearchPedidos : System.Web.UI.Page
    {
        IOrganizationService Service = null;
        
        Guid AccountId = Guid.Empty;
        Guid PassangerId = Guid.Empty;
        EntityReferenceCollection PassengerCollections = new EntityReferenceCollection();
        EntityReferenceCollection ParesCollections = new EntityReferenceCollection();  // for associate in connection
        EntityReferenceCollection NewParentsCollections = new EntityReferenceCollection(); //for associate in pare grid 

        Guid PassatgerRole = new Guid("879a39c7-b2d8-e611-8106-3863bb360098");
        Guid ParesRole = new Guid("5ef747a3-b1d8-e611-8106-3863bb360098");
        Guid AccountRole = new Guid("4c223fb7-b6d8-e611-8106-3863bb360098");

        TextInfo cultureInfo = Thread.CurrentThread.CurrentCulture.TextInfo;

        public IOrganizationService GetOrgService()
        {
            CrmServiceClient conn = new Microsoft.Xrm.Tooling.Connector.CrmServiceClient(ConfigurationManager.ConnectionStrings["conString"].ToString());
            Service = (IOrganizationService)conn.OrganizationWebProxyClient != null ? (IOrganizationService)conn.OrganizationWebProxyClient : (IOrganizationService)conn.OrganizationServiceProxy;


            return Service;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            GetOrgService();
        }

        private Entity GetPedidosByNumber(string orderNum)
        {
            Entity _entity = null;
            var query = new QueryExpression("salesorder");
            query.ColumnSet = new ColumnSet(true);
            query.Criteria.AddCondition("ordernumber", ConditionOperator.Equal, orderNum);
            EntityCollection _coll = Service.RetrieveMultiple(query);
            if (_coll != null && _coll.Entities.Count > 0)
            {
                _entity = _coll.Entities[0];
            }
            return _entity;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                string lblOrder = lblOrd.Text;
                string txtOrder = txtOrd.Text;
                string ddlOrder = ddlOrd.SelectedItem.Text;

                string OrderNumber = lblOrder + "-" + txtOrder + "-" + ddlOrder;
                if (OrderNumber != null)
                {
                    Entity Order = GetPedidosByNumber(OrderNumber);
                    if (Order != null && Order.Id != Guid.Empty)
                    {
                        BindallCountryRecords();
                        BindallNationalityRecords();
                        GetallOptionSet_Especilitat();
                        GetOptionsSet_AlimentariesEspecfiques(Service, "contact", "dfi_alimentariesespecfiques");
                        GetOptionsSet_AlimentariesAllrgia(Service, "contact", "dfi_alimentariesallrgia");


                        hdn_OrderId.Value = Convert.ToString(Order.Id);

                        string itineariohtmlText = Order.Attributes.Contains("new_elteuitinerari") && Order.Attributes["new_elteuitinerari"] != null ? Order.Attributes["new_elteuitinerari"].ToString() : "";
                        client2.Text = Order.Attributes.Contains("customerid") && Order.Attributes["customerid"] != null ? ((EntityReference)Order.Attributes["customerid"]).Name.ToString() : "";
                        Itinerario2.Text = Order.Attributes.Contains("new_itinerario") && Order.Attributes["new_itinerario"] != null ? ((EntityReference)Order.Attributes["new_itinerario"]).Name.ToString() : "";
                        Salida2.Text = Order.Attributes.Contains("new_fechasalida") && Order.Attributes["new_fechasalida"] != null ? Convert.ToString(((DateTime)Order.Attributes["new_fechasalida"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)) : "";
                        legada2.Text = Order.Attributes.Contains("new_fechallegada") && Order.Attributes["new_fechallegada"] != null ? Convert.ToString(((DateTime)Order.Attributes["new_fechallegada"]).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)) : "";
                        ArribadaDate.Value = Order.Attributes.Contains("new_fechallegada") && Order.Attributes["new_fechallegada"] != null ? Convert.ToString(((DateTime)Order.Attributes["new_fechallegada"]).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture)) : "";
                        Organitzador2.Text = Order.Attributes.Contains("new_contacto") && Order.Attributes["new_contacto"] != null ? ((EntityReference)Order.Attributes["new_contacto"]).Name.ToString() : "";
                        string preudelviatge = Order.Attributes.Contains("dfi_preudelviatge") && Order.Attributes["dfi_preudelviatge"] != null ? ((Money)Order.Attributes["dfi_preudelviatge"]).Value.ToString() : "";
                        bool dfi_autoritzaci = Order.Attributes.Contains("dfi_autoritzaci") && Order.Attributes["dfi_autoritzaci"] != null ? (bool)Order.Attributes["dfi_autoritzaci"] : false;
                        hdn_accountId.Value = Order.Attributes.Contains("customerid") && Order.Attributes["customerid"] != null ? Convert.ToString(((EntityReference)Order.Attributes["customerid"]).Id) : Convert.ToString(Guid.Empty);

                        FooterDiv.Visible = true;


                        if (preudelviatge != "")
                        {
                            viatge1.Visible = true;
                            viatge2.Text = ":" + preudelviatge + "€";
                            viatge2.Visible = true;
                        }

                        content.Visible = true;
                        divSearchSection.Visible = false;


                    }
                    else

                    {
                        lblNofound.Visible = true;
                        lblNofound.Text = "Registre no es troba, Si us plau, introduïu el número de comanda correcta.";

                    }

                }
            }

            catch (Exception ex)
            {


            }
        }

        private void BindallCountryRecords()
        {

            var query = new QueryExpression("new_country");
            query.ColumnSet = new ColumnSet("new_country");
            query.Orders.Add(new OrderExpression("new_country", OrderType.Ascending));
            EntityCollection _coll = Service.RetrieveMultiple(query);
            if (_coll != null && _coll.Entities.Count > 0)
            {
                foreach (var record in _coll.Entities)
                {
                    if (record.Attributes.Contains("new_country") && record.Attributes["new_country"] != null)
                    {
                        string Name = Convert.ToString(record.Attributes["new_country"]);
                        string Id = Convert.ToString((Guid)record.Id);
                        if (Name != null && Id != null)
                        {
                            if (Name == "Espanya")
                            {
                                pas_ddlCountry.Items.FindByText("Espanya").Value = Id;
                            }

                            else
                            {
                                pas_ddlCountry.Items.Add(new ListItem
                                {
                                    Value = Id,
                                    Text = Name
                                });
                            }
                        }
                    }
                }
            }
        }

        private void BindallNationalityRecords()
        {

            var query = new QueryExpression("new_nationality");
            query.ColumnSet = new ColumnSet("new_nationalitycatalan");
            query.Orders.Add(new OrderExpression("new_nationalitycatalan", OrderType.Ascending));
            EntityCollection _coll = Service.RetrieveMultiple(query);
            if (_coll != null && _coll.Entities.Count > 0)
            {
                foreach (var record in _coll.Entities)
                {
                    if (record.Attributes.Contains("new_nationalitycatalan") && record.Attributes["new_nationalitycatalan"] != null)
                    {
                        string Name = Convert.ToString(record.Attributes["new_nationalitycatalan"]);
                        string Id = Convert.ToString(record.Id);
                        if (Name != null && Id != null)
                        {
                            if (Name == "Espanyol")
                            {
                                pas_Nationality.Items.FindByText("Espanyol").Value = Id;
                            }
                            else
                            {
                                pas_Nationality.Items.Add(new ListItem
                                {
                                    Value = Id,
                                    Text = Name
                                });
                            }
                        }
                    }
                }
            }
        }




        protected void submit_Click(object sender, EventArgs e)
        {
            try
            {
                Guid OrderId = new Guid(hdn_OrderId.Value);

                EntityReference Customer = ((EntityReference)Service.Retrieve("salesorder", OrderId, new ColumnSet("customerid")).Attributes["customerid"]);

                #region Create Passanger contact record in CRM
                Entity Contact = new Entity("contact");

                int Ocupació = Convert.ToInt32(Pas_Ocupació.SelectedItem.Value);
                int especilitat = Convert.ToInt32(Pas_Especilitat.SelectedItem.Value);
                int Genere = Convert.ToInt32(pas_ddlGenere.SelectedItem.Value);
                string Nom = pas_Nom.Text;
                string Congnom1 = pas_Cognom1.Text;
                string Congnom2 = pas_Cognom2.Text;
                DateTime birth = DateTime.ParseExact(Birthdate.Text, "MM/dd/yyyy", null);
                Guid nationalityId = pas_Nationality.SelectedItem.Text != "" ? new Guid(pas_Nationality.SelectedItem.Value) : Guid.Empty;
                string nacionalitat = pas_Nationality.SelectedItem.Text.ToString();
                string email = pas_Email.Text;
                string postal = pas_CodigoPostal.Text;
                string telPrefix = pas_prefix.Text;
                string telefone = pas_Telefonemovil.Text;
                int doc = Convert.ToInt32(pas_ddlDoc.SelectedItem.Value);
                string noofdoc = pas_NoOfDoc.Text;
                Guid CountryId = pas_ddlCountry.SelectedItem.Text != "" ? new Guid(pas_ddlCountry.SelectedItem.Value) : Guid.Empty;
                DateTime Datacaducitat = DateTime.ParseExact(DatacaducitatDate.Text, "MM/dd/yyyy", null);
                bool bool_Necessitatsalimentaries = pas_Necessitatsalimentarie.SelectedItem.Text == "Si" ? true : false;
                int AlimentariesEspecifiquesValue = Convert.ToInt32(ddlAlimentariesEspecifiques.SelectedItem.Value);
                int AlimentariesAllrgiaValue = Convert.ToInt32(ddlAlimentariesAllrgia.SelectedItem.Value);
                string quina = txtQuina.Text != "" ? txtQuina.Text : "";
                bool Impediments = pas_Impediments.SelectedItem.Text == "Si" ? true : false;


                Guid NewPassangerId= CreatePassengerRecords(Customer, Ocupació, especilitat, Genere, Nom, Congnom1, Congnom2, birth, nationalityId, email, postal, telPrefix, telefone, doc, noofdoc, CountryId, Datacaducitat, bool_Necessitatsalimentaries, AlimentariesEspecifiquesValue, AlimentariesAllrgiaValue, quina, Impediments);

                #endregion

                # region Create Parent/Gardian Contact Records in CRM

                JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                var contactArray = (object)null;
                string contactlist = hdncontactGridData.Value;
                if (contactlist != null)
                {
                    contactArray = (ContactGridList[])json_serializer.Deserialize(contactlist, typeof(ContactGridList[]));

                    var contactRecords = ((ValuaTravel_Pedidos_CRM.SearchPedidos.ContactGridList[])contactArray);

                    if (contactRecords != null && contactRecords.Length > 0)
                    {
                        foreach (var record in contactRecords)
                        {
                            int pare_genere = Convert.ToInt32(record.Genere);
                            string pare_firstname = record.FName;
                            string pare_lastname = record.LName;
                            string pare_lastname1 = record.LName1;
                            int pare_doc = Convert.ToInt32(record.Doc);
                            string pare_NoOfDoc = record.NoofDoc;
                            string pare_email = record.Email;
                            string pare_telefone = record.Telefone;
                            CreateParentsRecords(pare_genere, pare_firstname, pare_lastname, pare_lastname1, pare_doc, pare_NoOfDoc, pare_email, pare_telefone);
                        }
                    }
                }
                #endregion

                #region Associate Passenger record with Account by Connection records
                if (PassangerId != Guid.Empty && OrderId != Guid.Empty)
                {
                    Entity Connection_Acc = new Entity("connection");
                    Connection_Acc.Attributes["record1id"] = new EntityReference("contact", PassangerId);
                    Connection_Acc.Attributes["record1roleid"] = new EntityReference("connectionrole", PassatgerRole);
                    Connection_Acc.Attributes["record2roleid"] = new EntityReference("connectionrole", AccountRole);
                    Connection_Acc.Attributes["record2id"] = new EntityReference("account", new Guid(hdn_accountId.Value));
                    Service.Create(Connection_Acc);
                }

                #endregion

                #region Associate Parents records with Order in pare grid
                if (NewParentsCollections.Count > 0 && OrderId != Guid.Empty)
                {
                    Relationship relationship = new Relationship("new_salesorder_contact_Pares");
                    Service.Associate("salesorder", OrderId, relationship, NewParentsCollections);
                }
                #endregion

                #region Associate Passenger record with Pares by Connection records
                if (ParesCollections.Count > 0 && PassangerId != Guid.Empty)
                {
                    foreach (var parecontact in ParesCollections)
                    {
                        Entity Connection_Pare = new Entity("connection");
                        Connection_Pare.Attributes["record1id"] = new EntityReference("contact", PassangerId);
                        Connection_Pare.Attributes["record1roleid"] = new EntityReference("connectionrole", PassatgerRole);
                        Connection_Pare.Attributes["record2roleid"] = new EntityReference("connectionrole", ParesRole);
                        Connection_Pare.Attributes["record2id"] = new EntityReference("contact", parecontact.Id);
                        Service.Create(Connection_Pare);
                    }
                }
                #endregion

                #region Update Pedidos record

                Entity OrderUpdate = new Entity("salesorder");

                if (OrderId != Guid.Empty)
                {
                    OrderUpdate.Id = OrderId;
                    int PassengersCount = GetallPassengersRecords(OrderId);
                    int TeachersCount = GetallTeachersRecords(OrderId);
                    OrderUpdate.Attributes["dfi_paxinscrites"] = PassengersCount + TeachersCount;
                    OrderUpdate.Attributes["dfi_enviarinscripci"] = true;
                    Service.Update(OrderUpdate);

                }

                #endregion

                #region Send Email notifiction with Attachment to contacts

                //byte[] result = GeneratePdfReport();

                //if (PassengerCollections.Count > 0)
                //{
                //   // SendEmailWithPdfAttachment(result, PassengerCollections);
                //}

                //if (ParesCollections.Count > 0)
                //{
                //  //  SendEmailWithPdfAttachment(result, ParesCollections);
                //}
                #endregion

                #region Send Email notifiction to passenger and parents

                if (NewPassangerId != Guid.Empty || NewParentsCollections!=null)
                {
                    SendEmailToPassenger(NewPassangerId, NewParentsCollections,  OrderId, Service);
                }

               
                #endregion

                Response.Redirect("Response.aspx", false);

            }
            catch (Exception ex)
            {

                Response.Redirect("Response.aspx", false);
            }
        }

        private byte[] GeneratePdfReport()
        {
            byte[] result;
            DataTable dtblTemp = new DataTable();
            dtblTemp.Columns.Add("ITINERARIO", typeof(string));
            dtblTemp.Columns.Add("FECHADESALIDA", typeof(string));
            dtblTemp.Columns.Add("FECHADERETORNO", typeof(string));
            dtblTemp.Columns.Add("ACCOUNT", typeof(string));

            DataRow drTemp = dtblTemp.NewRow();
            drTemp["ITINERARIO"] = Itinerario2.Text;
            drTemp["FECHADESALIDA"] = Salida2.Text;
            drTemp["FECHADERETORNO"] = legada2.Text;
            drTemp["ACCOUNT"] = client2.Text;
            dtblTemp.Rows.Add(drTemp);

            string strReportPath = Server.MapPath("~/Parent-Authorization.rdlc");

            string fileName = "Parent-Authorization";
            result = GetAttachmentBytes(strReportPath, dtblTemp, fileName);

            return result;
        }

        protected void pdfDownload_Click(object sender, ImageClickEventArgs e)
        {
            DataTable dtblTemp = new DataTable();
            dtblTemp.Columns.Add("ITINERARIO", typeof(string));
            dtblTemp.Columns.Add("FECHADESALIDA", typeof(string));
            dtblTemp.Columns.Add("FECHADERETORNO", typeof(string));
            dtblTemp.Columns.Add("ACCOUNT", typeof(string));

            DataRow drTemp = dtblTemp.NewRow();
            drTemp["ITINERARIO"] = Itinerario2.Text;
            drTemp["FECHADESALIDA"] = Salida2.Text;
            drTemp["FECHADERETORNO"] = legada2.Text;
            drTemp["ACCOUNT"] = client2.Text;
            dtblTemp.Rows.Add(drTemp);

            string strReportPath = Server.MapPath("~/Parent-Authorization.rdlc");

            string fileName = "Parent-Authorization";
            DownloadPdf(strReportPath, dtblTemp, fileName);



        }

        protected void DownloadPdf(string strReportPath, DataTable dtblTemp, string fileName)
        {
            byte[] bytes;
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Reset();

            ReportViewer1.LocalReport.EnableExternalImages = true;
            this.ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource objReportDataSource = new ReportDataSource("Pedidos_DataSet", dtblTemp.Copy());
            ReportViewer1.LocalReport.DataSources.Add(objReportDataSource);
            ReportViewer1.LocalReport.ReportPath = strReportPath;

            //ReportParameter rptParam = new ReportParameter("OrderId", Convert.ToString( OrderId));
            //ReportViewer1.LocalReport.SetParameters(rptParam);
            //rptParam = new ReportParameter("imageParam", @"C:\Users\deep.singh\Desktop\RDLCDemo\RDLCDemo\printer16x16.png");
            //ReportViewer1.LocalReport.SetParameters(rptParam);


            bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            Response.Buffer = true;
            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            try
            {

                Response.BinaryWrite(bytes);

            }
            catch (Exception ex)
            {

                Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Error while generating PDF.');", true);
                Console.WriteLine(ex.StackTrace);

            }

            //Response.Flush();
            //ReportViewer1.LocalReport.Refresh();
        }

        protected byte[] GetAttachmentBytes(string strReportPath, DataTable dtblTemp, string fileName)
        {
            byte[] bytes;
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.Reset();

            ReportViewer1.LocalReport.EnableExternalImages = true;
            this.ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;

            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource objReportDataSource = new ReportDataSource("Pedidos_DataSet", dtblTemp.Copy());
            ReportViewer1.LocalReport.DataSources.Add(objReportDataSource);
            ReportViewer1.LocalReport.ReportPath = strReportPath;

            //ReportParameter rptParam = new ReportParameter("OrderId", Convert.ToString( OrderId));
            //ReportViewer1.LocalReport.SetParameters(rptParam);
            //rptParam = new ReportParameter("imageParam", @"C:\Users\deep.singh\Desktop\RDLCDemo\RDLCDemo\printer16x16.png");
            //ReportViewer1.LocalReport.SetParameters(rptParam);


            bytes = ReportViewer1.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            //Response.Buffer = true;
            //Response.Clear();
            //Response.ContentType = mimeType;
            //Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
            try
            {
                return bytes;

                //Response.BinaryWrite(bytes);
                //SendEmailWithPdfAttachment(bytes);
            }
            catch (Exception ex)
            {

                Page.ClientScript.RegisterStartupScript(this.GetType(), "ErrorAlert", "alert('Error while generating PDF.');", true);
                Console.WriteLine(ex.StackTrace);
                return bytes;

            }

            //Response.Flush();
            //ReportViewer1.LocalReport.Refresh();
        }

        private void SendEmailWithPdfAttachment(byte[] result, EntityReferenceCollection ContactIds)
        {
            if (ContactIds.Count > 0)
            {
                foreach (var contact in ContactIds)
                {

                    Entity email = new Entity("email");
                    email.Attributes.Add("regardingobjectid", new EntityReference("salesorder", new Guid(hdn_OrderId.Value.ToString())));

                    //Creating EntityReference for from, to and cc. Need to be changed according to your requirement
                    EntityReference from = new EntityReference("systemuser", new Guid("D2D521A8-BE84-E211-A130-18A9057470F6"));
                    EntityReference to = new EntityReference("contact", contact.Id);

                    //Creating party list
                    Entity fromParty = new Entity("activityparty");
                    fromParty.Attributes.Add("partyid", from);
                    Entity toParty = new Entity("activityparty");
                    toParty.Attributes.Add("partyid", to);

                    EntityCollection collFromParty = new EntityCollection();
                    collFromParty.EntityName = "systemuser";
                    collFromParty.Entities.Add(fromParty);

                    EntityCollection collToParty = new EntityCollection();
                    collToParty.EntityName = "contact";
                    collToParty.Entities.Add(toParty);

                    // Adding from, to and cc to the email
                    email.Attributes.Add("from", collFromParty);
                    email.Attributes.Add("to", collToParty);

                    email.Attributes.Add("subject", "Here goes subject message.. : ");
                    email.Attributes.Add("description", "Here goes description text..");
                    Guid emailID = Service.Create(email); // Create the email

                    //// Attaching Pdf Report
                    //int NextActorID = new int();
                    //RetrieveEntityRequest request = new RetrieveEntityRequest();
                    //request.LogicalName = "email";
                    //RetrieveEntityResponse response = (RetrieveEntityResponse)Service.Execute(request);
                    //int objecttypecode = response.EntityMetadata.ObjectTypeCode.Value;

                    //Entity attachment = new Entity("activitymimeattachment");
                    //attachment.Attributes.Add("subject", "Report");
                    //attachment.Attributes.Add("filename", "Parent-Authorization.pdf");
                    //attachment.Attributes.Add("body", Convert.ToBase64String(result));
                    //attachment.Attributes.Add("filesize", Convert.ToInt32(result.Length.ToString()));
                    //attachment.Attributes.Add("mimetype", "application/pdf");
                    //attachment.Attributes.Add("attachmentnumber", NextActorID);
                    //attachment.Attributes.Add("objectid", new EntityReference("email", emailID));
                    //attachment.Attributes.Add("objecttypecode", objecttypecode);
                    //Service.Create(attachment); 

                    // Sending email
                    //SendEmailRequest reqSendEmail = new SendEmailRequest();
                    //reqSendEmail.EmailId = emailID;
                    //reqSendEmail.TrackingToken = "";
                    //reqSendEmail.IssueSend = true;
                    //SendEmailResponse res = (SendEmailResponse)Service.Execute(reqSendEmail);

                }
            }
        }

        private Guid CreatePassengerRecords(EntityReference customer, int Ocupació, int especilitat, int genere, string Nom, string Congnom1, string Congnom2, DateTime birthdate, Guid nationalityId, string email, string postal, string telPrefix, string telefone, int doc, string noofdoc, Guid CountryId, DateTime Datacaducitat, bool bool_Necessitatsalimentaries, int AlimentariesEspecifiques, int AlimentariesAllrgia, string quina, bool Impediments)
        {
            Guid OrderId = new Guid(hdn_OrderId.Value);

            var query = new QueryExpression("contact");
            query.ColumnSet = new ColumnSet(true);
            query.Criteria.AddCondition("dfi_noofdoc", ConditionOperator.Equal, noofdoc);
            EntityCollection _coll = Service.RetrieveMultiple(query);

            if (_coll.Entities.Count == 0)
            {
                Entity Contact = new Entity("contact");

                Contact.Attributes["dfi_tipocontacto"] = new OptionSetValue(1);
                if (Ocupació != -1) Contact.Attributes["dfi_profesion"] = new OptionSetValue(Ocupació);
                if (Ocupació == 2 && customer.Id != Guid.Empty) { Contact.Attributes["parentcustomerid"] = customer; }
                if (Ocupació != 6) Contact.Attributes["new_especialidad"] = new OptionSetValue(especilitat);
                Contact.Attributes["gendercode"] = new OptionSetValue(genere);
                Contact.Attributes["firstname"] = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Nom);
                Contact.Attributes["lastname"] = CultureInfo.CurrentCulture.TextInfo.ToTitleCase((Congnom1 + " " + Congnom2));
                Contact.Attributes["birthdate"] = birthdate;
                //Contact.Attributes["dfi_nacionalitat"] = nacionalitat;
                if (nationalityId != Guid.Empty) Contact.Attributes["dfi_nacionalitatid"] = new EntityReference("new_nationality", nationalityId);
                Contact.Attributes["emailaddress1"] = email;
                Contact.Attributes["address1_postalcode"] = postal;
                Contact.Attributes["new_telprefix"] = telPrefix;
                Contact.Attributes["mobilephone"] = telefone;
                Contact.Attributes["dfi_doc"] = new OptionSetValue(doc);
                Contact.Attributes["dfi_noofdoc"] = noofdoc;
                if (CountryId != Guid.Empty) Contact.Attributes["dfi_paisexpedition"] = new EntityReference("new_country", CountryId);
                Contact.Attributes["dfi_datacaducitat"] = Datacaducitat;
                Contact.Attributes["dfi_necessitatsalimentariesespecifique"] = bool_Necessitatsalimentaries;
                if (AlimentariesEspecifiques != -1) Contact.Attributes["dfi_alimentariesespecfiques"] = new OptionSetValue(AlimentariesEspecifiques);
                if (AlimentariesAllrgia != -1) Contact.Attributes["dfi_alimentariesallrgia"] = new OptionSetValue(AlimentariesAllrgia);
                Contact.Attributes["dfi_quina"] = quina;
                Contact.Attributes["dfi_impediments"] = Impediments;
                Contact.Attributes["donotemail"] = false;

                PassangerId = Service.Create(Contact);

                if (PassangerId != Guid.Empty)
                {
                    if (Ocupació == 6)
                    {
                        PassengerCollections.Add(new EntityReference("contact", PassangerId));
                        Relationship relationship = new Relationship("new_salesorder_contact_Travelers");
                        Service.Associate("salesorder", OrderId, relationship, PassengerCollections);
                    }

                    else
                    {
                        PassengerCollections.Add(new EntityReference("contact", PassangerId));
                        Relationship relationship = new Relationship("sonade_salesorder_contact");
                        Service.Associate("salesorder", OrderId, relationship, PassengerCollections);

                    }
                }               
            }

            else
            {
                PassengerCollections.Add(new EntityReference("contact", _coll.Entities[0].Id));
            }

            return PassangerId;

        }

        private void CreateParentsRecords(int genere, string firstName, string lastname, string lastname1, int doc, string noOfDoc, string email, string mobilephone)
        {
            Guid OrderId = new Guid(hdn_OrderId.Value);

            var query = new QueryExpression("contact");
            query.ColumnSet = new ColumnSet(true);
            query.Criteria.AddCondition("dfi_noofdoc", ConditionOperator.Equal, noOfDoc);
            EntityCollection _coll = Service.RetrieveMultiple(query);

            if (_coll.Entities.Count == 0)
            {
                Entity en_contact = new Entity("contact");

                en_contact.Attributes["dfi_tipocontacto"] = new OptionSetValue(1);

                en_contact.Attributes["dfi_profesion"] = new OptionSetValue(8);

                en_contact.Attributes["gendercode"] = new OptionSetValue(genere);

                en_contact.Attributes["firstname"] = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(firstName);

                en_contact.Attributes["lastname"] = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(lastname + " " + lastname1);

                en_contact.Attributes["dfi_doc"] = new OptionSetValue(doc);

                en_contact.Attributes["dfi_noofdoc"] = Convert.ToString(noOfDoc);

                en_contact.Attributes["emailaddress1"] = Convert.ToString(email);

                en_contact.Attributes["mobilephone"] = Convert.ToString(mobilephone);

                en_contact.Attributes["donotemail"] = false;

                Guid en_contactId = Service.Create(en_contact);

                if (en_contactId != Guid.Empty)
                {
                    ParesCollections.Add(new EntityReference("contact", en_contactId));
                    NewParentsCollections.Add(new EntityReference("contact", en_contactId));                    
                }
            }

            else
            {
                ParesCollections.Add(new EntityReference("contact", _coll.Entities[0].Id));
            }

        }

        private void CreateNewCity(string OtherCity)
        {
            Entity City = new Entity("new_ciudad");
            City.Attributes["new_name"] = OtherCity;
            City.Attributes["dfi_otherspecify"] = true;
            Service.Create(City);

        }

        protected void Displaydata_Click(object sender, EventArgs e)
        {
            string ParentFirstName = FirstName.Value != "" ? FirstName.Value : "";
            string ParentLastName = LastName.Value != "" ? LastName.Value : "";
            string ParentLastName1 = LastName1.Value != "" ? LastName1.Value : "";
            string ParentNoOfDoc = NoOfDoc.Value != "" ? NoOfDoc.Value : "";

            string PassengerFirstName = pas_Nom.Text != "" ? pas_Nom.Text : "";
            string PassengerlastName = pas_Cognom1.Text != "" ? pas_Cognom1.Text : "";
            string PassengerlastName1 = pas_Cognom2.Text != "" ? pas_Cognom2.Text : "";
            string Pedidoitinenary = Itinerario2.Text;
            string PassengerDocNo = pas_NoOfDoc.Text;
            string PedidoSalida = Salida2.Text;
            string Pedidolegada = legada2.Text;
            string PedidoAccount = client2.Text;

            string bt = FormatDate(Birthdate.Text);


            string html_under18 = @"<div id='Wrapper' style='font-family:Calibri;font-size:10pt;text-align:justify;margin: 30px' >
        <div id='Header' style='height:100px'>
            <center><img src='http://217.112.83.26:8083/image/LOGO_TRAVEL.jpg' style='height:80px' /></center>
        </div>
        <div class='center' id='Container'>
            <section>
                <span><h3>AUTORITZACIÓ VIATGE MENOR D’EDAT</h3></span>
                <p>
                    1. Jo, " + ParentFirstName + @" " + ParentLastName + @" " + ParentLastName1 + @" amb document d’identitat número " + ParentNoOfDoc + @"  representant legal 
                    de " + PassengerFirstName + @" " + PassengerlastName + @" " + PassengerlastName1 + @" , menor d’edat, l’autoritzo a realitzar el viatge a " + Pedidoitinenary + @" amb
                    data de sortida el " + PedidoSalida + @" i tornada el " + Pedidolegada + @", organitzat per " + PedidoAccount + @" amb organització tècnica de l’agència
                    de viatges VALUA TRAVEL, S.L., amb domicili a C/ Sant Josep nº 4, 08302 Mataró i inscrita com agència de viatges al Registre 
                    de Turisme de Catalunya sota el nº d’inscripció GC 2685.
                </p>
            </section>
            <section>
                <p>
                    2. Em dono per informat/del contingut de les condicions generals del viatge publicades al web: <br />
                 <a target='_blank' href='http://valuatravel.com/assets/uploads/files/docs/7798f-condicions-generals-del-viatge-combinat.pdf'>http://valuatravel.com/assets/uploads/files/docs/7798f-condicions-generals-del-viatge-combinat.pdf</a>
                </p>
            </section>

            <section>
                <p>
                    3. Autoritzo a " + PedidoAccount + @" a actuar en nom i representació de " + PassengerFirstName + @" " + PassengerlastName + @" " + PassengerlastName1 + @" ,
                    menor d’edat, envers l’agència VALUA TRAVEL, S.L. en tot allò relatiu a la contractació del viatge de referència i rebre en nom i 
                    representació de " + PassengerFirstName + @" " + PassengerlastName + @" " + PassengerlastName1 + @" tota la informació i documentació del viatge, 
                    així com a signar en nom i representació de " + PassengerFirstName + @" " + PassengerlastName + @" " + PassengerlastName1 + @"  el pertinent
                    contracte de viatge combinat.
                </p>
            </section>

            <section>
                <p>
                    4. Que als efectes del que disposa l’article 34.e) de la Llei Orgànica de Protecció de Dades, dono el meu consentiment de
                    forma implícita a la inscripció i per tant en la contractació del viatge, per mitjà del contracte signat en nom i representació del
                    meu fill/a, per el/la representant de " + PedidoAccount + @", o en reclamacions a aquest servei, per a que l’agència de viatges VALUA TRAVEL, 
                    S.L. pugui transferir les dades necessàries del meu fill/a per a reserves d’hotels, vols, transports i serveis contractats, als 
                    destinataris que sigui pertinents (majoristes, centrals de reserves, companyies aèries i navilieres o altres similars, i, si així fos 
                    necessari segons el servei o viatge a efectuar, amb destinació a qualsevol país del món, incloent, si procedeix, a aquells que no
                    ofereixen un nivell de protecció equiparable a l’exigida per la LOPD.<br /><br />
                    Les dades personals s’incorporaran i tractaran en un fitxer automatitzat propietat de VALUA TRAVEL SL , notificat a l’Agencia
                    Española de Protección de Datos amb el número 2131050117 que reuneix les mesures de seguretat necessàries per a garantir 
                    la total seguretat de les dades. En compliment de la normativa, que disposa l'article 34.e) LOPD el titular de les dades pot 
                    exercir els drets d’accés, cancel·lació, rectificació i oposició que reconeix la Llei Orgànica de protecció de dades posant-se en
                    contacte amb: VALUA TRAVEL SL. L’agencia queda facultada per a utilitzar les dades durant la vigència del contracte i per a la 
                    informació dels seus serveis.
                </p>
            </section>

        </div>
    </div>";


            string html_above18 = @"<div id='Wrapper' style='font-family:Calibri;font-size:10pt;text-align:justify'>
        <div id='Header' style='height:100px'>
            <center><img src='http://217.112.83.26:8083/image/LOGO_TRAVEL.jpg' style='height:80px' /></center>
        </div>
        <div class='center' id='Container' >
            <section>
                <span><h3>AUTORITZACIÓ ADULT</h3></span>
                <p >
                    1. Jo, " + PassengerFirstName + @" " + PassengerlastName + @" " + PassengerlastName1 + @" amb document d’identitat número " + PassengerDocNo + @" , autoritzo a " + PedidoAccount + @" a actuar en nom i representació meva  envers l’agència VALUA TRAVEL, S.L. en tot allò relatiu a
                    la contractació del viatge a " + Pedidoitinenary + @" amb data de sortida el " + PedidoSalida + @" i tornada el " + Pedidolegada + @", organitzat per:
                    " + PedidoAccount + @" amb organització tècnica de l’agència de viatges VALUA TRAVEL, S.L., amb domicili a C/ Sant Josep nº 4, 08302 
                    Mataró i inscrita com agència de viatges al Registre de Turisme de Catalunya sota el nº d’inscripció GC 2685, i a rebre tota la 
                    informació i documentació del viatge, així com a signar en nom i representació meva el pertinent contracte de viatge 
                    combinat.
                </p>



            </section>
            <section>
                <p>
                    2. Em dono per informat/del contingut de les condicions generals del viatge publicades al web: <br />
                    <a target='_blank' href='http://valuatravel.com/assets/uploads/files/docs/7798f-condicions-generals-del-viatge-combinat.pdf'> http://valuatravel.com/assets/uploads/files/docs/7798f-condicions-generals-del-viatge-combinat.pdf </a>
                </p>
            </section>

            <section>
                <p>
                    3. A efectes del que disposa l’article 34.e) de la Llei Orgànica de Protecció de Dades, dono el meu consentiment de forma
                    implícita a la inscripció i per tant en la contractació del viatge, per mitjà del contracte signat en nom i representació meva al 
                    representant de " + PedidoAccount + @", o en reclamacions a aquest servei, per a que l’agència de viatges VALUA TRAVEL, S.L. pugui 
                    transferir les dades necessàries per a reserves d’hotels, vols, transports i serveis contractats, als destinataris que sigui 
                    pertinents (majoristes, centrals de reserves, companyies aèries i navilieres o altres similars, i, si així fos necessari segons el 
                    servei o viatge a efectuar, amb destinació a qualsevol país del món, incloent, si procedeix, a aquells que no ofereixen un nivell 
                    de protecció equiparable a l’exigida per la LOPD.<br /><br />
                    Les dades personals s’incorporaran i tractaran en un fitxer automatitzat propietat de VALUA TRAVEL SL , notificat a l’Agencia 
                    Española de Protección de Datos amb el número 2131050117 que reuneix les mesures de seguretat necessàries per a garantir 
                    la total seguretat de les dades. En compliment de la normativa, que disposa l'article 34.e) LOPD el titular de les dades pot 
                    exercir els drets d’accés, cancel·lació, rectificació i oposició que reconeix la Llei Orgànica de protecció de dades posant-se en 
                    contacte amb: VALUA TRAVEL SL. L’agencia queda facultada per a utilitzar les dades durant la vigència del contracte i per a la 
                    informació dels seus serveis.

                </p>
            </section>
        </div>
    </div>";

            if (Birthdate.Text != "")
            {
                int PassengerBirthDate = OnCalculateAge(Birthdate.Text);

                if (PassengerBirthDate < 18)
                {
                    dynamicdata.InnerHtml = WebUtility.HtmlDecode(html_under18);
                }
                else
                {
                    dynamicdata.InnerHtml = WebUtility.HtmlDecode(html_above18);
                }

                Main.Visible = false;
                AUTORITZACIÓ.Visible = true;
                AUTORITZACIÓ.Focus();

                

            }
        }

        private int OnCalculateAge(string birthdate)
        {
            var currentYear = DateTime.Today.Year;

            var BirthYear = Convert.ToInt64(birthdate.Split('/')[2]);

            int age = Convert.ToInt32((currentYear - BirthYear));

            return age;
        }

        private int GetallPassengersRecords(Guid OrderId)
        {
            int count = 0;
            QueryExpression _query = new QueryExpression("contact");
            _query.ColumnSet.AddColumn("contactid");
            var link = _query.AddLink("new_salesorder_contact_travelers", "contactid", "contactid");
            link.LinkCriteria = new FilterExpression()
            {
                Conditions = { new ConditionExpression("salesorderid", ConditionOperator.Equal, OrderId) }
            };
            EntityCollection _coll = Service.RetrieveMultiple(_query);
            if (_coll != null && _coll.Entities.Count > 0)
            {
                count = _coll.Entities.Count;
            }
            return count;
        }

        private int GetallTeachersRecords(Guid OrderId)
        {
            int count = 0;
            QueryExpression _query = new QueryExpression("contact");
            _query.ColumnSet.AddColumn("contactid");
            var link = _query.AddLink("sonade_salesorder_contact", "contactid", "contactid");
            link.LinkCriteria = new FilterExpression()
            {
                Conditions = { new ConditionExpression("salesorderid", ConditionOperator.Equal, OrderId) }
            };
            EntityCollection _coll = Service.RetrieveMultiple(_query);
            if (_coll != null && _coll.Entities.Count > 0)
            {
                count = _coll.Entities.Count;
            }
            return count;
        }

        public class ContactGridList
        {
            public int Genere { get; set; }
            public string FName { get; set; }
            public string LName { get; set; }
            public string LName1 { get; set; }
            public string Doc { get; set; }
            public string NoofDoc { get; set; }
            public string Postal { get; set; }
            public string Email { get; set; }
            public string Telefone { get; set; }

        }


        private void GetallOptionSet_Especilitat()
        {

            RetrieveOptionSetRequest retrieveOptionSetRequest = new RetrieveOptionSetRequest { Name = "new_especialidad" };
            // Execute the request.
            RetrieveOptionSetResponse retrieveOptionSetResponse = (RetrieveOptionSetResponse)Service.Execute(retrieveOptionSetRequest);

            OptionSetMetadata retrievedOptionSetMetadata = (OptionSetMetadata)retrieveOptionSetResponse.OptionSetMetadata;

            OptionMetadata[] optionList = retrievedOptionSetMetadata.Options.ToArray();

            if (optionList.Length > 0)
            {
                foreach (var option in optionList)
                {
                    string value = Convert.ToString(option.Value);
                    var text = option.Label.UserLocalizedLabel.Label;

                    Pas_Especilitat.Items.Add(new ListItem
                    {
                        Value = value,
                        Text = text
                    });
                }
            }
        }

        private void GetOptionsSet_AlimentariesEspecfiques(IOrganizationService service, string entityName, string attributeName)
        {

            RetrieveAttributeRequest retrieveAttributeRequest = new
            RetrieveAttributeRequest
            {

                EntityLogicalName = entityName,

                LogicalName = attributeName,

                RetrieveAsIfPublished = true

            };
            // Execute the request.
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.

            Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)

            retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = string.Empty;
            foreach (OptionMetadata oMD in optionList)
            {
                string value = Convert.ToString(oMD.Value);
                string text = oMD.Label.UserLocalizedLabel.Label;
                ddlAlimentariesEspecifiques.Items.Add(new ListItem
                {
                    Value = value,
                    Text = text
                });
            }
        }

        private void GetOptionsSet_AlimentariesAllrgia(IOrganizationService service, string entityName, string attributeName)
        {

            RetrieveAttributeRequest retrieveAttributeRequest = new
            RetrieveAttributeRequest
            {

                EntityLogicalName = entityName,

                LogicalName = attributeName,

                RetrieveAsIfPublished = true

            };
            // Execute the request.
            RetrieveAttributeResponse retrieveAttributeResponse = (RetrieveAttributeResponse)service.Execute(retrieveAttributeRequest);
            // Access the retrieved attribute.

            Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata retrievedPicklistAttributeMetadata = (Microsoft.Xrm.Sdk.Metadata.PicklistAttributeMetadata)

            retrieveAttributeResponse.AttributeMetadata;// Get the current options list for the retrieved attribute.
            OptionMetadata[] optionList = retrievedPicklistAttributeMetadata.OptionSet.Options.ToArray();
            string selectedOptionLabel = string.Empty;
            foreach (OptionMetadata oMD in optionList)
            {
                string value = Convert.ToString(oMD.Value);
                string text = oMD.Label.UserLocalizedLabel.Label;
                ddlAlimentariesAllrgia.Items.Add(new ListItem
                {
                    Value = value,
                    Text = text
                });
            }
        }


        private void SendEmailToPassenger(Guid contactId, EntityReferenceCollection Parents, Guid orderId, IOrganizationService service)
        {    
            Entity contact = service.Retrieve("contact", contactId, new ColumnSet("firstname", "donotemail"));
            bool doNotAllow = (bool)contact.Attributes["donotemail"];
            Entity Order = service.Retrieve("salesorder", orderId, new ColumnSet("ordernumber", "new_itinerario"));
            string Itinerary = Order.Attributes["new_itinerario"] != null ? ((EntityReference)Order.Attributes["new_itinerario"]).Name : "";

            var targetEmailTemplateId = new Guid("E7714872-C433-E711-812F-E0071B65FE11");
            Entity template = GetTemplate(orderId, targetEmailTemplateId, "salesorder", service, null);
            string description = template.Contains("description") ? template.Attributes["description"].ToString() : "";
            string Subject = template.Contains("subject") ? template.Attributes["subject"].ToString() : "";

            Entity email = new Entity("email");

            Entity fromParty = new Entity("activityparty");
            //fromParty.Attributes["partyid"] = new EntityReference("systemuser", new Guid("D2D521A8-BE84-E211-A130-18A9057470F6"));  //9B27D36A-2C1A-E311-9A2F-18A905741066 (Pep)
            fromParty.Attributes["partyid"] = new EntityReference("queue", new Guid("626075AE-8134-E711-812F-E0071B665101"));

            Entity toParty = new Entity("activityparty");
            toParty.Attributes["partyid"] = new EntityReference("contact", contactId);

            if (Parents.Count > 0)
            {
                EntityCollection CCPartyColl = new EntityCollection();
                foreach (var cc in Parents)
                {
                    Entity ccParty = new Entity("activityparty");
                    ccParty.Attributes["partyid"] = cc;
                    CCPartyColl.Entities.Add(ccParty);
                }

                if (CCPartyColl.Entities.Count > 0)
                {
                    email.Attributes["cc"] = CCPartyColl;

                }
            }
            
            email.Attributes["to"] = new Entity[] { toParty };
            email.Attributes["from"] = new Entity[] { fromParty };            
            email.Attributes["regardingobjectid"] = new EntityReference("salesorder", orderId);
            email.Attributes["subject"] = HTMLToText(Subject);
            email.Attributes["description"] = description;
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{PassangerName}}", pas_Nom.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Itinerary}}", Itinerary);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Ocupació}}", Pas_Ocupació.SelectedItem.Text);
            if (Pas_Ocupació.SelectedItem.Value != "6")
            { email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Especilitat}}", Pas_Especilitat.SelectedItem.Text); }
            else { email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Especilitat}}", "N/A"); }
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{PassangerName}}", pas_Nom.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Gènere}}", pas_ddlGenere.SelectedItem.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Nom}}", pas_Nom.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Cognom1}}", pas_Cognom1.Text);
            email.Attributes["description"] = pas_Cognom2.Text != "" ? email.Attributes["description"].ToString().Replace("{{Cognom2}}", pas_Cognom2.Text) : email.Attributes["description"].ToString().Replace("{{Cognom2}}", "N/A");
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Datanaixement}}", FormatDate(Birthdate.Text));
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{nacionalitat}}", pas_Nationality.SelectedItem.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Correuelectronic}}", pas_Email.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Codipostal}}", pas_CodigoPostal.Text);
            email.Attributes["description"] = pas_Telefonemovil.Text != "" ? email.Attributes["description"].ToString().Replace("{{Telefone}}", pas_Telefonemovil.Text) : email.Attributes["description"].ToString().Replace("{{Telefone}}", "N/A");
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Document}}", pas_ddlDoc.SelectedItem.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Númerodeldocument}}", pas_NoOfDoc.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{País}}", pas_ddlCountry.SelectedItem.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Datadecaducitat}}", FormatDate(DatacaducitatDate.Text));
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Necessitats}}", pas_Necessitatsalimentarie.SelectedItem.Text);
            
            if (pas_Necessitatsalimentarie.SelectedItem.Value == "1")
            {
                string multiChoices = "";
                if (ddlAlimentariesEspecifiques.SelectedItem.Value == "3" || ddlAlimentariesEspecifiques.SelectedItem.Value == "4")
                {
                    multiChoices = ddlAlimentariesEspecifiques.SelectedItem.Text + ", " + ddlAlimentariesAllrgia.SelectedItem.Text;

                    if (ddlAlimentariesAllrgia.SelectedItem.Value == "11") {
                        multiChoices = multiChoices + ", " + txtQuina.Text;
                    }
                }
                else {
                    multiChoices = ddlAlimentariesEspecifiques.SelectedItem.Text;
                }
              
                email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Alimentaries}}", multiChoices);
            }
            else {
                email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Alimentaries}}", "");
                email.Attributes["description"] = email.Attributes["description"].ToString().Replace(",", "");
            }
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Impediments}}", pas_Impediments.SelectedItem.Text);

            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{SecordPage}}", dynamicdata.InnerHtml);

            email.Attributes["directioncode"] = true;
            Guid _emailId = service.Create(email);

            if (!doNotAllow)
            {
                SendEmailRequest req = new SendEmailRequest();
                req.EmailId = _emailId;
                req.IssueSend = true;
                req.TrackingToken = "";
                SendEmailResponse res = (SendEmailResponse)service.Execute(req);
            }
        }

        private string FormatDate(string _Date)
        {
            // DateTime Dt = DateTime.Now;
            //// IFormatProvider mFomatter = new System.Globalization.CultureInfo("es-ES");
            // Dt = DateTime.Parse(_Date);
            // return Dt.ToString("dd/MM/yyyy");
            string strDate = _Date.Substring(3, 2) + "/" + _Date.Substring(0, 2) + "/" + _Date.Substring(6, 4);
            return strDate;

        }

        private void SendEmailToParents(Guid contactId, Guid orderId, IOrganizationService service)
        {
            //contactId = new Guid("2F57EA03-E433-E711-812F-E0071B65FE11");
            Entity contact = service.Retrieve("contact", contactId, new ColumnSet("firstname", "donotemail"));
            bool doNotAllow = (bool)contact.Attributes["donotemail"];
            Entity Order = service.Retrieve("salesorder", orderId, new ColumnSet("ordernumber", "new_itinerario"));
            string Itinerary = Order.Attributes["new_itinerario"] != null ? ((EntityReference)Order.Attributes["new_itinerario"]).Name : "";

            var targetEmailTemplateId = new Guid("E7714872-C433-E711-812F-E0071B65FE11");
            Entity template = GetTemplate(orderId, targetEmailTemplateId, "salesorder", service, null);
            string description = template.Contains("description") ? template.Attributes["description"].ToString() : "";
            string Subject = template.Contains("subject") ? template.Attributes["subject"].ToString() : "";

            Entity fromParty = new Entity("activityparty");
            //fromParty.Attributes["partyid"] = new EntityReference("systemuser", new Guid("D2D521A8-BE84-E211-A130-18A9057470F6"));  //9B27D36A-2C1A-E311-9A2F-18A905741066 (Pep)
            fromParty.Attributes["partyid"] = new EntityReference("queue", new Guid("626075AE-8134-E711-812F-E0071B665101"));

            Entity toParty = new Entity("activityparty");
            toParty.Attributes["partyid"] = new EntityReference("contact", contactId);

            Entity email = new Entity("email");
            email.Attributes["to"] = new Entity[] { toParty };
            email.Attributes["from"] = new Entity[] { fromParty };
            email.Attributes["regardingobjectid"] = new EntityReference("salesorder", orderId);
            email.Attributes["subject"] = HTMLToText(Subject);
            email.Attributes["description"] = description;
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{PassangerName}}", pas_Nom.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Itinerary}}", Itinerary);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Ocupació}}", Pas_Ocupació.SelectedItem.Text);
            if (Pas_Ocupació.SelectedItem.Value != "6")
            { email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Especilitat}}", Pas_Especilitat.SelectedItem.Text); }
            else { email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Especilitat}}", "N/A"); }
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{PassangerName}}", pas_Nom.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Gènere}}", pas_ddlGenere.SelectedItem.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Nom}}", pas_Nom.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Cognom1}}", pas_Cognom1.Text);
            email.Attributes["description"] = pas_Cognom2.Text != "" ? email.Attributes["description"].ToString().Replace("{{Cognom2}}", pas_Cognom2.Text) : email.Attributes["description"].ToString().Replace("{{Cognom2}}", "N/A");
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Datanaixement}}", Birthdate.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{nacionalitat}}", pas_Nationality.SelectedItem.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Correuelectronic}}", pas_Email.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Codipostal}}", pas_CodigoPostal.Text);
            email.Attributes["description"] = pas_Telefonemovil.Text != "" ? email.Attributes["description"].ToString().Replace("{{Telefone}}", pas_Telefonemovil.Text) : email.Attributes["description"].ToString().Replace("{{Telefone}}", "N/A");
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Document}}", pas_ddlDoc.SelectedItem.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Númerodeldocument}}", pas_NoOfDoc.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{País}}", pas_ddlCountry.SelectedItem.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Datadecaducitat}}", DatacaducitatDate.Text);
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Necessitats}}", pas_Necessitatsalimentarie.SelectedItem.Text);

            if (pas_Necessitatsalimentarie.SelectedItem.Value == "1")
            {
                string multiChoices = "";
                if (ddlAlimentariesEspecifiques.SelectedItem.Value == "3" || ddlAlimentariesEspecifiques.SelectedItem.Value == "4")
                {
                    multiChoices = ddlAlimentariesEspecifiques.SelectedItem.Text + ", " + ddlAlimentariesAllrgia.SelectedItem.Text;

                    if (ddlAlimentariesAllrgia.SelectedItem.Value == "11")
                    {
                        multiChoices = multiChoices + ", " + txtQuina.Text;
                    }
                }
                else
                {
                    multiChoices = ddlAlimentariesEspecifiques.SelectedItem.Text;
                }

                email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Alimentaries}}", multiChoices);
            }
            else
            {
                email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Alimentaries}}", "");
                email.Attributes["description"] = email.Attributes["description"].ToString().Replace(",", "");
            }
            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{Impediments}}", pas_Impediments.SelectedItem.Text);

            email.Attributes["description"] = email.Attributes["description"].ToString().Replace("{{SecordPage}}", dynamicdata.InnerHtml);

            email.Attributes["directioncode"] = true;
            Guid _emailId = service.Create(email);

            if (!doNotAllow)
            {
                SendEmailRequest req = new SendEmailRequest();
                req.EmailId = _emailId;
                req.IssueSend = true;
                req.TrackingToken = "";
                SendEmailResponse res = (SendEmailResponse)service.Execute(req);
            }
        }

        public Entity GetTemplate(Guid entityId, Guid templateId, string entityName, IOrganizationService service, ITracingService trace)
        {
            Entity template = null;
            try
            {
                InstantiateTemplateRequest instTemplateReq = new InstantiateTemplateRequest
                {
                    TemplateId = templateId,
                    ObjectId = entityId,
                    ObjectType = entityName
                };
                InstantiateTemplateResponse instTemplateResp = (InstantiateTemplateResponse)service.Execute(instTemplateReq);
                if (instTemplateResp != null)
                {
                    template = instTemplateResp.EntityCollection.Entities[0];

                }
                return template;
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException("GetTemplateDescription :", ex);
            }
        }

        public string HTMLToText(string htmlString)
        {
            string ss = string.Empty;
            Regex regex = new Regex("\\<[^\\>]*\\>");
            ss = regex.Replace(htmlString, String.Empty);
            return ss;// Plain Text as a OUTPUT
        }

    }
}