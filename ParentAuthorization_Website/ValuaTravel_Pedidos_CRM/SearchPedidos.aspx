<%@ Page Language="C#"  AutoEventWireup="true" CodeBehind="SearchPedidos.aspx.cs"  Inherits="ValuaTravel_Pedidos_CRM.SearchPedidos" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    
    <title></title>
       <%--  <meta charset="utf-8" />--%>
<%--  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>--%>

                    <script src="Jquery/Jquery-3.1.1.js"></script>                   
                 <script src="Jquery/combodate.js"></script>
                 <script src="Jquery/Moment.js"></script>

    <style type="text/css">
    .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Calibri;
        font-size: 11pt;
        border: 5px solid #808080;
        width: 400px;
        height: 200px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }
</style>

    <style>

       /*@media screen and (max-width:480px) and (min-width: 320px) {
      .day {
        width:40px!important;
        }
        .month {
            width:40px!important;
        }
        .year {
            width: 40px!important;
        }
            /*.Telfone {            
             width: 150px!important;
            }*/

     

        /*@media screen and (max-width: 768px) and (min-width: 481px) {
       .day {
        width: 70px!important;
        }
        .month {
            width: 70px!important;
        }
        .year {
            width: 70px!important;
        }
    } 

        @media screen and (max-width: 1024px) and (min-width: 789px) {
       .day {
        width: 87px!important;
        }
        .month {
            width: 87px!important;
        }
        .year {
            width: 87px!important;
        }
    }*/ 

     /*@media screen and (max-width: 1368px) and (min-width: 1025px) {
       .day {
        width: 97px!important;
        }
        .month {
            width: 98px!important;
        }
        .year {
            width: 98px!important;
        }

        .Telfone {
            
             width: 223px!important;
            }
    }*/ 
    

        body {
        padding: 0!important;
        margin:0 !important;
        } 
         
        @font-face {
            font-family: "AvenirNextLTPro-Regular.otf";
            font-style: normal;
            src: url(AvenirNextLTPro-Regular.otf); /*if IE */
            src: local("AvenirNextLTPro-Regular.otf"), url("AvenirNextLTPro-Regular.otf") format("truetype"); /* non-IE */
        }

         .day {
        width: 97px!important;
        }
        .month {
            width: 97px!important;
        }
        .year {
            width: 97px!important;
        }


        body.custom_font {
            font-family: Calibri; /*AvenirNextLTPro-Regular;*/ /* no .ttf */
            font-size:11pt;
        } 
        
        td {
            font-size: 11pt;
        }

        input[type=text], input[type=email], input[type=Phone],
        input[type=date], select {
            width: 100%;           
            padding: 5px 20px;
            margin: 5px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        input[type=submit], button[type=button] {
            background-color: maroon;           
            color: lightblue;
            padding: 5px 20px;
            margin: 5px 0;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type=submit]:hover, button[type=button]:hover {
                background-color: #45a049;
            }

       

        .requiredcss::after {
            content: '*';
            margin-right: 4px;
            color: red;
        }

        p.p1 {
            margin-bottom: .0001pt;           
            margin-left: 0in;
            margin-right: 0in;
            margin-top: 0in;
        }
        
        </style>
 
    <script>
        $(document).ready(function () {


            
            $("#submit").click(function () {                    
                if (document.getElementById("<%=chkSubmit.ClientID %>").checked == true) {
                    ShowProgress();
                }             
            })

           
            var dobObject = $("#Birthdate").val();
            if (dobObject == "") {
                $("#PARE").hide();
            }
                      

            $('#Birthdate').combodate({
                minYear: 1950,
                maxYear: 2050,
                yearDescending: false
            });

            $('#DatacaducitatDate').combodate({
                minYear: 1950,
                maxYear: 2050,
                yearDescending: false
            });

            $('#Birthdate').change(function () {
                if ($('#Birthdate').val() != "") {                  
                    var birthDate = $('#Birthdate').val();                  
                    var age = CalcualteYears(birthDate);                    
                    if (parseInt(age) > 18) {
                            $("#PARE").hide();
                        }
                        else {
                            $("#PARE").show();
                        }
                }
                else {
                  
                     $("#PARE").hide();
                   }
            })

            


            
            var selected = $('option:selected', "#Pas_Ocupaci�").text();
            if (selected == "") {
                $(".Especilitat").hide();
                
            }
            else {
                $(".Especilitat").show();
            }

            $(function () {
                $("#Pas_Ocupaci�").change(function () {
                    var selectedText = $('option:selected', this).text();
                    if (selectedText == "ESTUDIANT") {
                        $(".Especilitat").hide();
                        $('option:selected', "#Pas_Especilitat").removeAttr("selected");
                    }
                    else {
                        $(".Especilitat").show();
                    }
                });
            });
                    

        
            // pas_Necessitatsalimentarie -- hide/show
            var selected = $('option:selected', "#pas_Necessitatsalimentarie").text();
            if (selected == "") {
                $("#ddlAlimentariesEspecifiques").hide();
                $("#ddlAlimentariesAllrgia").hide();
                $("#Rf_ddlAlimentaries").hide();
                $("#Rf_ddlAllrgia").hide();
                $("#Rf_txtQuina").hide();                

                $("#txtQuina").val('');
            }

            $(function () {
                $("#pas_Necessitatsalimentarie").change(function () {
                    var selectedText = $('option:selected', this).text();
                    if (selectedText == "Si") {
                        $("#ddlAlimentariesEspecifiques").show();
                        $("#Rf_ddlAlimentaries").show();
                        document.getElementById("Rf_ddlAlimentaries").enabled = true;

                    
                    }
                    else {
                        $("#ddlAlimentariesEspecifiques").hide();                        
                        $("#ddlAlimentariesAllrgia").hide();
                        $("#txtQuina").hide();
                        $('option:selected', "#ddlAlimentariesEspecifiques").removeAttr("selected");
                        $('option:selected', "#ddlAlimentariesAllrgia").removeAttr("selected");                        
                        $("#Rf_ddlAlimentaries").hide();
                        $("#Rf_ddlAllrgia").hide();
                        $("#Rf_txtQuina").hide();
                        $("#txtQuina").val('');
                        document.getElementById("Rf_ddlAlimentaries").enabled = false;
                        document.getElementById("Rf_ddlAllrgia").enabled = false;
                        document.getElementById("Rf_txtQuina").enabled = false;

                      
                    }
                });
            });

            // ddlAlimentariesEspecifiques -- hide/show
            var selected = $('option:selected', "#ddlAlimentariesEspecifiques").text();
            if (selected == "") {
                $("#ddlAlimentariesAllrgia").hide();
                $("#Rf_ddlAllrgia").hide();
                
            }
            else {
                $("#ddlAlimentariesAllrgia").show();
                $("#Rf_ddlAllrgia").hide();
                document.getElementById("Rf_ddlAllrgia").enabled = false;
            }
            $(function () {
                $("#ddlAlimentariesEspecifiques").change(function () {
                    var selectedText = $('option:selected', this).text();
                    if (selected == "") {
                        $("#ddlAlimentariesAllrgia").hide();
                        $("#txtQuina").hide();
                        $("#txtQuina").val('');
                      
                    }

                    if (selectedText == "Al�l�rgia" || selectedText == "Intoler�ncia") {
                        $("#ddlAlimentariesAllrgia").show();
                        $("#Rf_ddlAllrgia").show();
                        document.getElementById("Rf_ddlAllrgia").enabled = true;

                    }
                    else {
                        $("#ddlAlimentariesAllrgia").hide();
                        $("#txtQuina").hide();
                        $("#txtQuina").val('');
                        $("#Rf_ddlAllrgia").hide();
                        $("#Rf_txtQuina").hide();
                        document.getElementById("Rf_ddlAllrgia").enabled = false;
                        document.getElementById("Rf_txtQuina").enabled = false;
                    }
                });
            });

            //Quina TextBox
            var selected = $('option:selected', "#ddlAlimentariesAllrgia").text();
            if (selected == "") {
                $("#txtQuina").hide();
                $("#Rf_txtQuina").hide();
             }

            $(function () {
                $("#ddlAlimentariesAllrgia").change(function () {
                    var selectedText = $('option:selected', this).text();
                    if (selectedText == "Altres") {
                        $("#txtQuina").val('');
                        $("#txtQuina").show();
                        $("#Rf_txtQuina").show();
                        document.getElementById("Rf_txtQuina").enabled = true;

                    }
                    else {  
                        $("#txtQuina").hide();
                        $("#Rf_txtQuina").hide();
                        document.getElementById("Rf_txtQuina").enabled = false;

                    }
                });
            });
                       

            $(".addCF").click(function () {
               
               var rowCount = $('.rowtbgrid').length + 1;
               var Row = '<tr class="rowtbgrid"><th scope="row"></th><td><select class="Fc-Genere required_attr"  id="Genere' + rowCount + '" style="width:130px"><option value="1">Home</option><option value="2">Dona</option></select> </td><td><input type="text" id="FirstName' + rowCount + '" class="Fc-FirstName required_attr" value="" placeholder="Nom" required="true" /></td><td><input type="text"  id="LastName' + rowCount + '" class="Fc-LastName required_attr" value="" placeholder="Cognom 1" required="true"/></td><td><input type="text"  id="LastName1' + rowCount + '" class="Fc-LastName1" value="" placeholder="Cognom 2"/></td><td><select class="Fc-Doc" id="Doc' + rowCount + '" style="width:130px"><option value="1">DNI</option><option value="2">Passaport</option></select> </td><td><input type="text" style="width:140px"  id="NoOfDoc' + rowCount + '" class="Fc-NoOfDoc required_attr" value="" MaxLength="09"  placeholder="Num Document" required="true"/><td><input type="email" id="Email' + rowCount + '" class="Fc-Email required_attr" value="" placeholder="correu electr�nic" required="true"/></td><td><input type="phone" id="Telefone' + rowCount + '" class="Fc-Telefone"  min="0" max="9999999999999" value="" placeholder="M�vil"/></td><td><button type="button" class="remCF" style="color:white"  >-</button></td></tr>';

                $('#tbGrid').append(Row);

            });

            $("#tbGrid").on('click', '.remCF', function () {
                $(this).parent().parent().remove();
            });

            

            $("#btnContinuar").click(function () {
         

                if ($("#DatacaducitatDate").val() != "") {
                    document.getElementById('hdncontactGridData').value = JSON.stringify(getAllContactGridData());
                    var dob = $("#Birthdate").val() != "" ? $("#Birthdate").val() : "";
                    if (dob != "") {
                        var age = CalcualteYears(dob);
                        if (parseInt(age) > 18) {
                            RemoveFocus();
                        }
                        else {
                            AddRequired();
                        }
                    }
                }

            })

            $('#txtOrd').on('keyup', function () {
                this.value = this.value.replace(/[^0-9+]/g, '')
            });
            $('#pas_CodigoPostal').on('keyup', function () {
                this.value = this.value.replace(/[^0-9+]/g, '')
            });
            

            $('input[type="phone"]').each(function () {
                $(this).on('keyup', function () {

                    if ($(this).val() > Number($(this).attr("max"))) {
                        val = $(this).val().slice(0, $(this).attr("max").length);
                        $(this).val(val);
                    }
                    this.value = this.value.replace(/[^0-9+]/g, '')                  
                });
            });


            function ShowProgress() {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.addClass("modal");
                    $('body').append(modal);
                    var loading = $(".loading");
                    loading.show();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }


            function getAllContactGridData() {
                var data = []; 
                    $('.rowtbgrid').each(function () {
                        var genere = $(this).find('.Fc-Genere').val();
                        var firstName = $(this).find('.Fc-FirstName').val() != "" ? $(this).find('.Fc-FirstName').val() : "";
                        var lastName = $(this).find('.Fc-LastName').val() != "" ? $(this).find('.Fc-LastName').val() : "";
                        var lastName1 = $(this).find('.Fc-LastName1').val() != "" ? $(this).find('.Fc-LastName1').val() : "";
                        var doc = $(this).find('.Fc-Doc').val();
                        var noOfDoc = $(this).find('.Fc-NoOfDoc').val();
                        var email = $(this).find('.Fc-Email').val();
                        var telefone = $(this).find('.Fc-Telefone').val();

                        if (firstName != "" && lastName != "" && noOfDoc!="") {
                            var alldata = {
                                'Genere': genere,
                                'FName': firstName,
                                'LName': lastName,
                                'LName1': lastName1,
                                'Doc': doc,
                                'NoofDoc': noOfDoc,
                                'Email': email,
                                'Telefone': telefone
                            }
                            data.push(alldata);
                        }
                        else{
                            RemoveFocus();
                        }

                    });
                return data;
            }                         

            function CalcualteYears(dob) {              
                var currentyear = new Date().getFullYear();
                var BirthYear = dob.split('/')[2];              
                var age = currentyear - BirthYear;              
                return age;
            }
        });

        function DisplayReportData() {
            if ($("#pas_Nom").val() != "" && $("#pas_Cognom1").val() != "" && $("#Birthdate").val() != "" ) {
                $("#DisplayData").show();
            }

            else {
                $("#DisplayData").hide();
            }

        }

        function AddRequired() {
            $('.required_attr').attr("required", true);
        }

        function RemoveRequired() {
            $('.required_attr').removeAttr("required");
            
        }

        function RemoveFocus() {       
            $('.Fc-Genere').removeAttr("required");
            $('.Fc-FirstName').removeAttr("required");
            $('.Fc-LastName').removeAttr("required"); 
            $('.Fc-LastName1').removeAttr("required");
            $('.Fc-Doc').removeAttr("required");
            $('.Fc-NoOfDoc').removeAttr("required");
            $('.Fc-Email').removeAttr("required");
            $('.Fc-Telefone').removeAttr("required");            
        }

        function validateExpirydate(sender, args) {
            if ($("#DatacaducitatDate").val() != "" && $("#ArribadaDate").val() != "") {
                var Datacaducitat = Date.parse($('#DatacaducitatDate').val());
                var returnDate = Date.parse($("#ArribadaDate").val());
                if (Datacaducitat <= returnDate) {
                    args.IsValid = false;              
                }
                else {
                    args.IsValid = true;                    
                }
            }           
        }
            function ValidateMultiCheckBox1(sender, args) {
                if (document.getElementById("<%=chkSubmit.ClientID %>").checked == true) {
                    args.IsValid = true;
                } else {
                    args.IsValid = false;
                   
                }
            }


            function ValidateMultiCheckBox2(sender, args) {
                if (document.getElementById("<%=chk2.ClientID %>").checked == true) {
                    args.IsValid = true;
                } else {
                    args.IsValid = false;
                }
            }
        

    </script>

    

</head>
<body  class="custom_font">
    <form id="form1" runat="server"  >
        <asp:ScriptManager ID="ScriptManager1" runat="server">  </asp:ScriptManager>
        <asp:HiddenField ID="hdn_OrderId" runat="server" Visible="false"/>
        <asp:HiddenField ID="hdn_accountId" runat="server" Visible="false"/>
      
        <div id="HeaderDiv" runat="server" >         
         <img src="Image/AFIMAGEN_CABECERA_TRAVEL_1_copia.jpg" style="width:100%" height:"100%"/>
        </div>
        <div class="container" style="margin-top:20px;margin-left:30px;margin-right:30px">
                <div id="Main" runat="server">
            <div id="divSearchSection" runat="server"> 
            <asp:label Text="Introduir el n�mero de reserva (exemple: ORD-0111-2016) :" runat="server" Font-Size="Large" ></asp:label>&nbsp;
            <asp:Label ID="lblOrd" runat="server" Font-Bold="true" Text="ORD"></asp:Label> - <asp:TextBox ID="txtOrd" Width="80px" MaxLength="5" runat="server"></asp:TextBox> -
                <asp:DropDownList Width="95px" ID="ddlOrd" runat="server"><asp:ListItem Text="2016" Value="2016"></asp:ListItem><asp:ListItem Text="2017" Value="2017"></asp:ListItem></asp:DropDownList>            
            &nbsp; &nbsp;<asp:Button ID="Button1" CausesValidation="False"  runat="server" OnClientClick="RemoveRequired()"  OnClick="Button1_Click" Text="Cerca"  />
            <br /><br />
             <asp:label ForeColor="Red" id="lblNofound"  runat="server" Font-Size="Large" visible="false"></asp:label>
            <br  /></div>
            <hr />  
             <div id="content" runat="server" visible="false">
            
             <asp:Label Font-Size="X-Large" ID="pedidoInfo_label" runat="server" Font-Bold="true">DADES DEL VIATGE</asp:Label> <br /><br />
                <div id="OrderData" class="orderdata" runat="server">
                 <table >
                    <tr>
                        <td  style="width:235px">
                    <asp:Label ID="client1"  runat="server" >Client</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="client2" runat="server" ></asp:Label><br />
                        </td>
                    </tr>                    
                    <tr>
                        <td style="width:235px">
<asp:Label ID="Itinerario1"  runat="server">Itinerari  </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Itinerario2" runat="server" ></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:235px">
                <asp:Label ID="Salida1" runat="server">Sortida </asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Salida2" runat="server" ></asp:Label><br />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:235px">

 <asp:Label ID="legada1" runat="server">Arribada</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="legada2" runat="server" ></asp:Label><br /><asp:HiddenField ID="ArribadaDate" runat="server" ></asp:HiddenField>
                        </td>

                    </tr>
                    <tr>
                        <td style="width:235px">

 <asp:Label ID="Organitzador1" runat="server">Organitzador</asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="Organitzador2" runat="server" ></asp:Label><br />
                        </td>

                    </tr>
                       <tr>
                        <td style="width:235px">
 <asp:Label ID="viatge1" runat="server" Font-Size="20px"  Visible="false" >Preu del viatge</asp:Label>
                        </td>
                        <td>
 <asp:Label ID="viatge2" runat="server" Visible="false"></asp:Label><br />
                        </td>

                    </tr>
                </table>
 </div>
              <br />  
         
                 <asp:Label Font-Size="X-Large" ID="PASSATGER_label" runat="server" Font-Bold="true">IDENTIFICACI� DEL PASSATGER</asp:Label>               
                <div id="Passatger"><br />                  
                    <strong>IMPORTANT</strong>: les dades que est�s apunt d'introduir han de coincidir amb les  dades que apareixen en el teu document  d'identitat o Passaport.<br /> 
                    <br /> 
                     <table > 
                         <tr>
                        <td style="width:400px">
                    <asp:Label ID="Ocupaci�_label"  runat="server" class="requiredcss">Ocupaci�</asp:Label>
                        </td>
                        <td  style="width:300px">
                             <asp:DropDownList ID="Pas_Ocupaci�" runat="server" >
                                 <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                 <asp:ListItem Text="MESTRE PRIMARIA" Value="1"></asp:ListItem>
                                 <asp:ListItem Text="PROFESSOR/A SECUNDARIA" Value="2"></asp:ListItem>
                                 <asp:ListItem Text="ESTUDIANT" Value="6"></asp:ListItem>
                             </asp:DropDownList>                            
                           <br />
                        </td>
                       <td>                                  
                             <strong> <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ForeColor="Red" ControlToValidate="Pas_Ocupaci�" InitialValue="-1" runat="server" ErrorMessage="&nbsp Seleccioneu ocupaci�"></asp:RequiredFieldValidator> </strong>                                
                             
                             </td>
                    </tr> 
                            <tr class="Especilitat" >
                        <td >
                    <asp:Label ID="Especilitat_label"  runat="server" class="requiredcss">Especilitat</asp:Label>
                        </td>
                        <td >
                             <asp:DropDownList ID="Pas_Especilitat" runat="server" >
                                 <asp:ListItem Text="" Value="-1"></asp:ListItem>
                             </asp:DropDownList>                            
                           <br />
                        </td>
                      
                    </tr>                        
                    <tr>
                        <td >
                    <asp:Label ID="genere_label"  runat="server" class="requiredcss">G�nere</asp:Label>
                        </td>
                        <td>
                             <asp:DropDownList ID="pas_ddlGenere" runat="server" >
                                 <asp:ListItem Text="Home" Value="1"></asp:ListItem>
                                 <asp:ListItem Text="Dona" Value="2"></asp:ListItem>
                             </asp:DropDownList>                            
                           <br />
                        </td>
                      
                    </tr>    
                          <tr>
                        <td>
                <asp:Label ID="Nom_label" runat="server" class="requiredcss">Nom</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox  Class="required_attr " ID="pas_Nom" runat="server" required="true"  ></asp:TextBox><br />
                        
                        </td>
                    </tr>  
                    <tr>
                        <td>
                <asp:Label ID="Cognom1_label" runat="server" class="requiredcss">Cognom 1</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox  Class="required_attr" ID="pas_Cognom1" runat="server" required="true" ></asp:TextBox><br />
                        
                        </td>
                    </tr>
                    <tr>
                          <td>
                <asp:Label ID="Cognom2_label" runat="server" class="requiredcss" >Cognom 2</asp:Label>
                        </td>
                        <td>
                            <asp:TextBox Class="required_attr" ID="pas_Cognom2" runat="server" required="true" ></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                            <td>
                 <asp:Label ID="BirthDate_label" runat="server" class="requiredcss">Data naixement</asp:Label>
                                        </td>
                                        <td>                            
                                            <asp:TextBox type="text" id="Birthdate" data-format="MM/DD/YYYY" data-template="D MMM YYYY" name="date" runat="server"  class= "required_attr "/>
                         </td>
                             <td>                                  
                                  <strong> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ForeColor="Red" ControlToValidate="Birthdate" InitialValue="" runat="server" ErrorMessage="&nbsp Si us plau seleccioneu les dates de naixement v�lida"></asp:RequiredFieldValidator> </strong>                                
                             
                             </td>
                       
                    </tr>
                    <tr>
                        <td>
 <asp:Label ID="Nationality_label" runat="server" class="requiredcss">Le teva nacionalitat</asp:Label>
                        </td>
                        <td> <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

                  <asp:DropDownList ID="pas_Nationality" AutoPostBack="True" runat="server"  class= "" >
                      <asp:ListItem Text="" Value="-1"></asp:ListItem>
                     <asp:ListItem Text="Espanyol"></asp:ListItem>
                  </asp:DropDownList>
        </ContentTemplate>

                             </asp:UpdatePanel>       
                        </td>
                        <td>   <strong> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ForeColor="Red" ControlToValidate="pas_Nationality" InitialValue="-1" runat="server" ErrorMessage="&nbsp Seleccioneu nacionalitat"></asp:RequiredFieldValidator> </strong>                                
                             </td>
                    </tr>  
                      <tr>
                          <td>

                          </td>
                          <td></td>
                      </tr>
                      <tr>
                              <td>
                                  <span style="font-size:larger; font-weight: 700;">DADES DE CONTACTE DEL PASSATGER</span></td>
                              
                          </tr>                     
                    <tr>
                        <td>
 <asp:Label ID="Email_label" runat="server" class="requiredcss" >Correu electr�nic</asp:Label>
                        </td>
                        <td>
 <asp:TextBox Class="required_attr "  ID="pas_Email" TextMode="Email" runat="server" required="true"></asp:TextBox><br />
                        </td>
                    </tr>
                    <tr>
                        <td>
 <asp:Label  ID="CodigoPostal_label" runat="server" class="requiredcss" >Codi postal</asp:Label>
                        </td>
                        <td>
 <asp:TextBox Class="required_attr " ID="pas_CodigoPostal" MaxLength="5"  runat="server" required="true"></asp:TextBox><br />
                        </td>
                    </tr>
                        <tr>
                        <td >
 <asp:Label ID="Telefonemovil_label"  runat="server">Tel�fon m�vil</asp:Label>
                        </td>
                        <td >
                            <asp:TextBox ID="pas_prefix" Text="+34" runat="server" Width="70px" MaxLength="4" class= ""  ></asp:TextBox>&nbsp;
                            <asp:TextBox ID="pas_Telefonemovil" value=""  Width="220px"  type="phone" MaxLength="9"   runat="server" ></asp:TextBox>
 <br /> 
                                        </td>
                            
                    </tr>
                      <tr>
                          <td> </td>
                          <td></td>
                      </tr>
                      <tr>
                              <td>
                                  <span style="font-size:larger; font-weight: 700;">DOCUMENTACI� DEL PASSATGER</span></td>
                          </tr>
                       <tr>
                          <td></td>
                          <td></td>
                      </tr>
                         <tr>
                        <td>
 <asp:Label ID="Doc_label" runat="server" class="requiredcss" >Document amb el que viatjar�s</asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="pas_ddlDoc" runat="server" class= "">
                                <asp:ListItem Text="DNI" Value="1">                                 
                                </asp:ListItem><asp:ListItem Text="Passaport" Value="2"></asp:ListItem>
                            </asp:DropDownList>
<br />
                        </td>
                    </tr>
                          <tr>
                        <td>
 <asp:Label  ID="NoOfDoc_label" runat="server"   class="requiredcss">N�mero del document</asp:Label>
                        </td>
                        <td>
 <asp:TextBox Class="required_attr "  ID="pas_NoOfDoc" MaxLength="09"  runat="server" required="true"></asp:TextBox><br />
                        </td>
                    </tr>
                          <tr>
                        <td>
 <asp:Label ID="PaisExpedition_label" runat="server"   class="requiredcss">Pa�s d'expedici� del document</asp:Label>
                        </td>
                        <td>
                             <asp:UpdatePanel ID="updt" runat="server">
    <ContentTemplate>        
            <asp:DropDownList ID="pas_ddlCountry" AutoPostBack="True" runat="server" class= "">
                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                <asp:ListItem Text="Espanya"></asp:ListItem>
            </asp:DropDownList>
         </ContentTemplate>
  </asp:UpdatePanel>               
         </td>
                              <td>   <strong> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="pas_ddlCountry" InitialValue="-1" runat="server" ErrorMessage="&nbsp Seleccioneu el pa�s"></asp:RequiredFieldValidator> </strong>                                
                             </td>
                    </tr>             
                          <tr>
                        <td>
 <asp:Label ID="Datacaducitat_label" runat="server" class="requiredcss" >Data de caducitat del document</asp:Label>
                        </td>
                        <td>                        
 <asp:TextBox type="text" id="DatacaducitatDate" data-format="MM/DD/YYYY" data-template="D MMM YYYY" name="date" runat="server" class= "required_attr "  />
                        </td><td>                     
                           <strong>  <asp:CustomValidator id="CustomValidator3" runat="server" 
  ControlToValidate = "DatacaducitatDate"  
  ClientValidationFunction="validateExpirydate" ErrorMessage="&nbsp;&nbsp;El document ha expirat, Si us plau, introdueixi document v�lid"  ForeColor="Red"  ></asp:CustomValidator></strong>

                             </td>
                    </tr>

                          <tr>
                          <td></td>
                          <td></td>
                      </tr>
                      <tr>
                              <td><span style="font-size:larger; font-weight: 700;">NECESSITATS ESPEC�FIQUES DEL PASSATGER</span></td>                              
                          </tr>   
                           <tr>
                        <td>
 <asp:Label ID="Necessitatsalimentaries_label"  runat="server"  class="requiredcss" >Necessitats aliment�ries especifiques</asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList  ID="pas_Necessitatsalimentarie" runat="server" class= "">
                                   <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                  <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                 <asp:ListItem Text="No" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                            <td>  
                            <strong> <asp:RequiredFieldValidator ID="RequiredFieldValidator5"  ForeColor="Red" ControlToValidate="pas_Necessitatsalimentarie" InitialValue="-1" runat="server" ErrorMessage="&nbsp Seleccioneu qualsevol opci�"></asp:RequiredFieldValidator> </strong>                                                                  
                        </td>
                    </tr>
                         <tr>
                        <td>
                     
                        </td>
                        <td>
                            <asp:DropDownList  ID="ddlAlimentariesEspecifiques" runat="server" >
                                   <asp:ListItem Text="" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                             <td>  
                            <strong> <asp:RequiredFieldValidator ID="Rf_ddlAlimentaries"  ForeColor="Red" ControlToValidate="ddlAlimentariesEspecifiques" InitialValue="-1" runat="server" ErrorMessage="&nbsp Seleccioneu qualsevol opci�"></asp:RequiredFieldValidator> </strong>                                                                  
                                           </td>
                        </tr>
                           <tr>
                        <td>
                      
                        </td>
                        <td>
                            <asp:DropDownList  ID="ddlAlimentariesAllrgia" runat="server" class= "">
                                   <asp:ListItem Text="" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                        </td> <td>  
                              <strong> <asp:RequiredFieldValidator ID="Rf_ddlAllrgia"  ForeColor="Red" ControlToValidate="ddlAlimentariesAllrgia" InitialValue="-1" runat="server" ErrorMessage="&nbsp Seleccioneu qualsevol opci�"></asp:RequiredFieldValidator> </strong>                                                                  
                     </td>
                        </tr>
                           <tr>
                             <td></td>
                             <td>
                                  <asp:TextBox ID="txtQuina" runat="server" placeholder="Quina" class= "" ></asp:TextBox><br />
                             </td>
                               <td>  
                              <strong> <asp:RequiredFieldValidator ID="Rf_txtQuina"  ForeColor="Red" ControlToValidate="txtQuina" InitialValue="" runat="server" ErrorMessage="&nbsp Por favor rellene este campo"></asp:RequiredFieldValidator> </strong>                                                                  
                       </td>
                         </tr>
                           <tr>
                             <td>

    <asp:Label class="requiredcss"  runat="server">Impediments f�sics o ps�quics a tenir en compte pel <br />desenvolupament del viatge </asp:Label>
                             </td>
                             <td>
                                 <asp:DropDownList ID="pas_Impediments" runat="server" required="true" class= "" >
                                   <asp:ListItem Text="" Value="-1"></asp:ListItem>
                                  <asp:ListItem Text="Si" Value="1"></asp:ListItem>
                                 <asp:ListItem Text="No" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                             </td>
                             <td>
                            <strong> <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ForeColor="Red" ControlToValidate="pas_Impediments" InitialValue="-1" runat="server" ErrorMessage="&nbsp Seleccioneu qualsevol opci�"></asp:RequiredFieldValidator> </strong>                                                                  

                             </td>
                          
                         </tr>
                </table>                                    

                </div>
            <br /> <br />
                               
                <div id="PARE" runat="server" >
                    <asp:Label Font-Size="X-Large"  ID="PARE_label" runat="server" Font-Bold="true">IDENTIFICACI� DEL PARE/MARE/TUTOR LEGAL</asp:Label> 
                    <br/>
                    <asp:HiddenField ID="hdncontactGridData"  runat="server" Value="" />   
    <table id="tbGrid" runat="server"  > 
	<tr class="rowtbgrid" >       
		<th scope="row"></th>
   <td><select class="Fc-Genere required_attr" id="Genere" style="width:130px"><option value="1">Home</option><option value="2">Dona</option></select> </td>         
   <td><input runat="server" type="text" id="FirstName"  class="Fc-FirstName required_attr" value="" placeholder="Nom" required="required" /> </td> 
   <td><input runat="server" type="text"  id="LastName" class="Fc-LastName required_attr" value="" placeholder="Cognom 1" required="required" /></td>  
    <td><input runat="server" type="text"  id="LastName1" class="Fc-LastName1" value="" placeholder="Cognom 2" /></td>  
   <td><select runat="server" class="Fc-Doc required_attr" id="Doc" style="width:130px"><option value="1">DNI</option><option value="2">Passaport</option></select> </td>      
   <td><input runat="server" type="text"  style="width:140px" id="NoOfDoc" class="Fc-NoOfDoc required_attr" value="" maxlength="09" placeholder="Num Document" required="required"/>  </td> 
   <td><input runat="server" type="email" id="Email" class="Fc-Email required_attr" value="" placeholder="correu electr�nic" required="required"/></td> 
   <td><input runat="server" type="phone" id="Telefone" class="Fc-Telefone" min="0" max="9999999999999" placeholder="M�vil" /></td>
         
			<td> <button type="button" class="addCF" style="color:white"  >+</button> <%--<a href="javascript:void(0);" class="addCF">Add</a>--%>
		            </td> </tr>
                    </table>
                </div>
                <br /><br />
             <asp:Label Font-Size="X-Large" ID="Documentacio_label" runat="server" Font-Bold="true">DOCUMENTACI� NECESSARIA</asp:Label>               
                
                 <div id="Documentacio">
                     <br />
                     <strong><span >Passatger menor d'edat</span></strong><span>: per viatjar fora del territori nacional cal que gestionis l'autoritzaci� paterna/materna expedida per la policia Nacional o Mossos d�Esquadra.</span><br />
                     <br />
                     <strong><span >Passatger amb nacionalitat espanyola o d'algun pa�s de la Uni� Europea</span></strong>:<br />
                     <br />
                     <span >&nbsp;&nbsp;&nbsp;
 
�	Vols amb destinaci� al mateix territori nacional (vols dom�stics): document d'identitat o passaport en vigor.</span><br  />
                     <span >&nbsp;&nbsp;&nbsp;
�	Vols amb destinaci� a pa�sos de la Uni� Europea (espai Schengen): document d'identitat o passaport en vigor.
                     </span>
                     <br />
                     <span >&nbsp;&nbsp;&nbsp; �	Vols destinaci� a pa�sos fora de la Uni� Europea: passaport en vigor +  visat en vigor per al pa�s de destinaci�, si es necessita.</span><br />
                     <br />
                     <strong><span>Passatger amb nacionalitat d'altres pa�sos del m�n:</span></strong>
                     <br />
                    
                     <span>&nbsp;&nbsp;&nbsp;
Vols dins de la Uni� Europea (inclosos vols dins d�Espanya) o fora dels pa�sos de la Uni� Europa (espai Schengen):<br />
                     &nbsp;&nbsp;&nbsp; - Passaport en vigor (amb m�nim de 3 mesos de validesa)
                     <br />
&nbsp;&nbsp;&nbsp; - NIE en vigor (amb m�nim de 3 mesos de validesa)
                                              <br />
&nbsp;&nbsp;&nbsp; - Visat per al pa�s de destinaci�, si fos necessari, en vigor.   
                     </span>
                     <br />

                     <br />
                     <br />

                     <img src="Image/attention%20logo.png" style="width:40px;margin-left: 20px;" /><br />
                   <strong>�s responsabilitat del passatger la gesti� i l�obtenci� dels documents necessaris per a viatjar. Tamb� �s responsabilitat del passatger disposar de la documentaci� amb la vig�ncia requerida en el moment del viatge.  Valua Travel no es far� responsable de problemes originats per l�incompliment d�aquests sup�sits.</strong> 
                      <br  /> <br /> 
                     </div>
                <br /> <br />
            <asp:Label Font-Size="X-Large" ID="TargetSanitaria_label" runat="server" Font-Bold="true">TARGETA SANIT�RIA EUROPEA</asp:Label>               
                <div id="TargetSanitaria">
                   <br />
                     Caldria disposar de la Targeta Sanit�ria Europea que podeu obtenir als Centres d�Atenci� Informativa de la Seguretat Social.  Si es disposa d�una targeta d�asseguran�a complement�ria (privada) tamb� �s bo dur-la. <br /><br />
                    <span>Com sol�licitar la Targeta Sanit�ria Europea: </span><br /><br />
                        <ul>
                        <li>Per-tel�fon: 901166565 <br /><br /></li>
                        <li>En persona (imprescindible cita previa): en aquest <a target="_blank" href="http://www.seg-social.es/Internet_1/Oficinas/index.htm">enlla�</a><br /><br /></li>
                       <li> Online: en aquest <a target="_blank" href="https://w6.seg-social.es/solTse/Inicio?Input1=Solicitar+%2F+Renovar++Tarjeta+Sanitaria">enlla�</a><br /></li>
                      </ul>
                     <br /> 
                    
                     <asp:CheckBox Class="required_attr" ID="chk2"  runat="server"/>&nbsp;Les dades que consten en aquest formulari son certes, correctes i responen en veracitat al document d�identitat o passaport
                    

                    <strong><asp:CustomValidator ID="CustomValidator2" runat="server"  ForeColor="Red" ErrorMessage=" Marqueu la casella per a poder continuar" ClientValidationFunction = "ValidateMultiCheckBox2" ></asp:CustomValidator></strong><br />
                <br />
                </div>
                <br />
                 <div  style="text-align:left">
                
                  <asp:Button runat="server" ID="btnContinuar" Font-Size="Large" Width="120px" Height="40px" Text="Continuar"  OnClick="Displaydata_Click"  /> 
                <br /><br /><br /> </div>

                 </div>
                </div>

                 <div id="AUTORITZACI�" runat="server" visible="false">
      <asp:Label Font-Size="X-Large" ID="AUTORITZACI�_label" runat="server" Font-Bold="true">AUTORITZACI�</asp:Label>   
                     <br /><br />
                        <div id="dynamicdata" runat="server" style="width:100%;height:400px;border-style:groove;overflow-y:scroll" >                           
                        </div>
                    <br />
                     <asp:CheckBox Class="required_attr" ID="chkSubmit"  runat="server"/>&nbsp;He llegit i accepto l�autoritzaci� 
                

                     <strong><asp:CustomValidator ID="CustomValidator1"  runat="server" ForeColor="Red" ErrorMessage=" Marqueu la casella per a poder continuar" ClientValidationFunction = "ValidateMultiCheckBox1" ></asp:CustomValidator></strong><br /> <br />
                     <asp:CheckBox Checked="true" ID="chkdefault"  runat="server"/>&nbsp;Vull rebre infomaci� sobre cursos d'ang�ls a l'estranger 
                     <br /><br />

                 <div  style="text-align:center">
                      <br />
                     <asp:Button  type="button" id="submit" Font-Size="Large" Text="Enviar Formulari" Height="40px" runat="server"  OnClick="submit_Click"/>
                 </div>
         
                </div >             
                         
                 <div id="FooterDiv" runat="server" >    
             <img src="Image/AF_FOOTER_general.jpg" runat="server" id="footerImage" style="width:100%;height:100%" /> 
                    
        </div>

        </div> 
        
        <!-- Modal Start here-->
     <div class="loading" align="center"><br />
    Estem gestionant la teva inscripci�, queda�t a l�espera. No premis cap bot�, no tanquis la finestra ni vagis enrere en el navegador.
   Espera a que el proc�s finalitzi.
<br />
    <br />
   <img src="Image/loader.gif" alt="" />
        </div>
        <!-- Modal ends Here -->             
          
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Height="1px" Width="1px" Visible="false"></rsweb:ReportViewer>
     
       
    </form>
  

</body>

</html>
